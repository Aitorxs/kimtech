<?php
/* Copyright (C) 2004-2018  Laurent Destailleur     <eldy@users.sourceforge.net>
 * Copyright (C) 2018-2019  Nicolas ZABOURI         <info@inovea-conseil.com>
 * Copyright (C) 2019-2020  Frédéric France         <frederic.france@netlogic.fr>
 * Copyright (C) 2021 		Armando Machuca			<sistemas@machfree.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * 	\defgroup   kimtech     Module Kimtech
 *  \brief      Kimtech module descriptor.
 *
 *  \file       htdocs/kimtech/core/modules/modKimtech.class.php
 *  \ingroup    kimtech
 *  \brief      Description and activation file for module Kimtech
 */
include_once DOL_DOCUMENT_ROOT.'/core/modules/DolibarrModules.class.php';
include_once(DOL_DOCUMENT_ROOT.'/core/class/hookmanager.class.php');
/**
 *  Description and activation class for module Kimtech
 */
class modKimtech extends DolibarrModules
{
	/**
	 * Constructor. Define names, constants, directories, boxes, permissions
	 *
	 * @param DoliDB $db Database handler
	 */
	public function __construct($db)
	{
		global $langs, $conf;
		$this->db = $db;

		// Id for module (must be unique).
		// Use here a free id (See in Home -> System information -> Dolibarr for list of used modules id).
		$this->numero = 900800; // TODO Go on page https://wiki.dolibarr.org/index.php/List_of_modules_id to reserve an id number for your module

		// Key text used to identify module (for permissions, menus, etc...)
		$this->rights_class = 'kimtech';

		// Family can be 'base' (core modules),'crm','financial','hr','projects','products','ecm','technic' (transverse modules),'interface' (link with external tools),'other','...'
		// It is used to group modules by family in module setup page
		$this->family = "Machfree";

		// Module position in the family on 2 digits ('01', '10', '20', ...)
		$this->module_position = '90';

		// Gives the possibility for the module, to provide his own family info and position of this family (Overwrite $this->family and $this->module_position. Avoid this)
		//$this->familyinfo = array('myownfamily' => array('position' => '01', 'label' => $langs->trans("MyOwnFamily")));
		// Module label (no space allowed), used if translation string 'ModuleKimtechName' not found (Kimtech is name of module).
		$this->name = preg_replace('/^mod/i', '', get_class($this));

		// Module description, used if translation string 'ModuleKimtechDesc' not found (Kimtech is name of module).
		$this->description = "KimtechDescription";
		// Used only if file README.md and README-LL.md not found.
		$this->descriptionlong = "KimtechDescription";

		// Author
		$this->editor_name = 'Machfree';
		$this->editor_url = 'https://www.machfree.com';

		// Possible values for version are: 'development', 'experimental', 'dolibarr', 'dolibarr_deprecated' or a version string like 'x.y.z'
		$this->version = '1.5';
		// Url to the file with your last numberversion of this module
		//$this->url_last_version = 'http://www.example.com/versionmodule.txt';

		// Key used in llx_const table to save module status enabled/disabled (where KIMTECH is value of property name of module in uppercase)
		$this->const_name = 'MAIN_MODULE_'.strtoupper($this->name);

		// Name of image file used for this module.
		// If file is in theme/yourtheme/img directory under name object_pictovalue.png, use this->picto='pictovalue'
		// If file is in module/img directory under name object_pictovalue.png, use this->picto='pictovalue@module'
		// To use a supported fa-xxx css style of font awesome, use this->picto='xxx'
		$this->picto = 'kimtech@kimtech';

		// Define some features supported by module (triggers, login, substitutions, menus, css, etc...)
		$this->module_parts = array(
			// Set this to 1 if module has its own trigger directory (core/triggers)
			'triggers' => 1,
			// Set this to 1 if module has its own login method file (core/login)
			'login' => 0,
			// Set this to 1 if module has its own substitution function file (core/substitutions)
			'substitutions' => 0,
			// Set this to 1 if module has its own menus handler directory (core/menus)
			'menus' => 0,
			// Set this to 1 if module overwrite template dir (core/tpl)
			'tpl' => 0,
			// Set this to 1 if module has its own barcode directory (core/modules/barcode)
			'barcode' => 0,
			// Set this to 1 if module has its own models directory (core/modules/xxx)
			'models' => 0,
			// Set this to 1 if module has its own printing directory (core/modules/printing)
			'printing' => 0,
			// Set this to 1 if module has its own theme directory (theme)
			'theme' => 0,
			// Set this to relative path of css file if module has its own css file
			'css' => array(
				   //'/kimtech/css/kimtech.css.php',
			),
			// Set this to relative path of js file if module must load a js on all pages
			'js' => array(
				   //'/kimtech/js/kimtech.js.php',
			),
			// Set here all hooks context managed by module. To find available hook context, make a "grep -r '>initHooks(' *" on source code. You can also set hook context to 'all'
			'hooks' => array('thirdpartycard','productcard', 'propalcard', 'supplier_proposalcard', 'ordercard','ordersuppliercard','invoicecard', 'invoicesuppliercard','projectcard','printFieldListWhere'),

			// Set this to 1 if features of module are opened to external users
			'moduleforexternal' => 0,
		);

		// Data directories to create when module is enabled.
		// Example: this->dirs = array("/kimtech/temp","/kimtech/subdir");
		$this->dirs = array("/kimtech/temp");

		// Config pages. Put here list of php page, stored into kimtech/admin directory, to use to setup module.
		$this->config_page_url = array("setup.php@kimtech");

		// Dependencies
		// A condition to hide module
		$this->hidden = false;
		// List of module class names as string that must be enabled if this module is enabled. Example: array('always1'=>'modModuleToEnable1','always2'=>'modModuleToEnable2', 'FR1'=>'modModuleToEnableFR'...)
		$this->depends = array();
		$this->requiredby = array(); // List of module class names as string to disable if this one is disabled. Example: array('modModuleToDisable1', ...)
		$this->conflictwith = array(); // List of module class names as string this module is in conflict with. Example: array('modModuleToDisable1', ...)

		// The language file dedicated to your module
		$this->langfiles = array("kimtech@kimtech");

		// Prerequisites
		$this->phpmin = array(5, 5); // Minimum version of PHP required by module
		$this->need_dolibarr_version = array(11, -3); // Minimum version of Dolibarr required by module

		// Messages at activation
		$this->warnings_activation = array(); // Warning to show when we activate module. array('always'='text') or array('FR'='textfr','ES'='textes'...)
		$this->warnings_activation_ext = array(); // Warning to show when we activate an external module. array('always'='text') or array('FR'='textfr','ES'='textes'...)
		//$this->automatic_activation = array('FR'=>'KimtechWasAutomaticallyActivatedBecauseOfYourCountryChoice');
		//$this->always_enabled = true;								// If true, can't be disabled

		// Constants
		// List of particular constants to add when module is enabled (key, 'chaine', value, desc, visible, 'current' or 'allentities', deleteonunactive)
		// Example: $this->const=array(1 => array('KIMTECH_MYNEWCONST1', 'chaine', 'myvalue', 'This is a constant to add', 1),
		//                             2 => array('KIMTECH_MYNEWCONST2', 'chaine', 'myvalue', 'This is another constant to add', 0, 'current', 1)
		// );
		$this->const = array();

		// Some keys to add into the overwriting translation tables
		/*$this->overwrite_translation = array(
			'en_US:ParentCompany'=>'Parent company or reseller',
			'fr_FR:ParentCompany'=>'Maison mère ou revendeur'
		)*/

		if (!isset($conf->kimtech) || !isset($conf->kimtech->enabled)) {
			$conf->kimtech = new stdClass();
			$conf->kimtech->enabled = 0;
		}

		// Array to add new pages in new tabs
		$this->tabs = array();
		// Example:
		// $this->tabs[] = array('data'=>'objecttype:+tabname1:Title1:mylangfile@kimtech:$user->rights->kimtech->read:/kimtech/mynewtab1.php?id=__ID__');  					// To add a new tab identified by code tabname1
		// $this->tabs[] = array('data'=>'objecttype:+tabname2:SUBSTITUTION_Title2:mylangfile@kimtech:$user->rights->othermodule->read:/kimtech/mynewtab2.php?id=__ID__',  	// To add another new tab identified by code tabname2. Label will be result of calling all substitution functions on 'Title2' key.
		// $this->tabs[] = array('data'=>'objecttype:-tabname:NU:conditiontoremove');                                                     										// To remove an existing tab identified by code tabname
		//
		// Where objecttype can be
		// 'categories_x'	  to add a tab in category view (replace 'x' by type of category (0=product, 1=supplier, 2=customer, 3=member)
		// 'contact'          to add a tab in contact view
		// 'contract'         to add a tab in contract view
		// 'group'            to add a tab in group view
		// 'intervention'     to add a tab in intervention view
		// 'invoice'          to add a tab in customer invoice view
		// 'invoice_supplier' to add a tab in supplier invoice view
		// 'member'           to add a tab in fundation member view
		// 'opensurveypoll'	  to add a tab in opensurvey poll view
		// 'order'            to add a tab in customer order view
		// 'order_supplier'   to add a tab in supplier order view
		// 'payment'		  to add a tab in payment view
		// 'payment_supplier' to add a tab in supplier payment view
		// 'product'          to add a tab in product view
		// 'propal'           to add a tab in propal view
		// 'project'          to add a tab in project view
		// 'stock'            to add a tab in stock view
		// 'thirdparty'       to add a tab in third party view
		// 'user'             to add a tab in user view

		// Dictionaries
		$this->dictionaries = array();
		if (! isset($conf->kimtech->enabled)) $conf->kimtech->enabled=0;	// This is to avoid warnings
			$this->dictionnaries=array(
					'langs'=>'kimtech@kimtech',
					'tabname'=>array(MAIN_DB_PREFIX."kimtech_marca"),		// List of tables we want to see into dictonnary editor
					'tablib'=>array("kimtech_marca"),													// Label of tables
					'tabsql'=>array('SELECT f.rowid as rowid, f.label,f.active FROM '.MAIN_DB_PREFIX.'kimtech_marca as f'),	// Request to select fields
					'tabsqlsort'=>array("label ASC"),																					// Sort order
					'tabfield'=>array("label"),																					// List of fields (result of select to show dictionnary)
					'tabfieldvalue'=>array("label"),																				// List of fields (list of fields to edit a record)
					'tabfieldinsert'=>array("label"),																			// List of fields (list of fields for insert)
					'tabrowid'=>array("rowid"),																									// Name of columns with primary key (try to always name it 'rowid')
					'tabcond'=>array($conf->kimtech->enabled)												// Condition to show each dictionnary
			);

		// Boxes/Widgets
		// Add here list of php file(s) stored in kimtech/core/boxes that contains a class to show a widget.
		$this->boxes = array(
			//  0 => array(
			//      'file' => 'kimtechwidget1.php@kimtech',
			//      'note' => 'Widget provided by Kimtech',
			//      'enabledbydefaulton' => 'Home',
			//  ),
			//  ...
		);

		// Cronjobs (List of cron jobs entries to add when module is enabled)
		// unit_frequency must be 60 for minute, 3600 for hour, 86400 for day, 604800 for week
		$this->cronjobs = array(
			//  0 => array(
			//      'label' => 'MyJob label',
			//      'jobtype' => 'method',
			//      'class' => '/kimtech/class/kimtech.class.php',
			//      'objectname' => 'Kimtech',
			//      'method' => 'doScheduledJob',
			//      'parameters' => '',
			//      'comment' => 'Comment',
			//      'frequency' => 2,
			//      'unitfrequency' => 3600,
			//      'status' => 0,
			//      'test' => '$conf->kimtech->enabled',
			//      'priority' => 50,
			//  ),
		);
		// Example: $this->cronjobs=array(
		//    0=>array('label'=>'My label', 'jobtype'=>'method', 'class'=>'/dir/class/file.class.php', 'objectname'=>'MyClass', 'method'=>'myMethod', 'parameters'=>'param1, param2', 'comment'=>'Comment', 'frequency'=>2, 'unitfrequency'=>3600, 'status'=>0, 'test'=>'$conf->kimtech->enabled', 'priority'=>50),
		//    1=>array('label'=>'My label', 'jobtype'=>'command', 'command'=>'', 'parameters'=>'param1, param2', 'comment'=>'Comment', 'frequency'=>1, 'unitfrequency'=>3600*24, 'status'=>0, 'test'=>'$conf->kimtech->enabled', 'priority'=>50)
		// );

		// Permissions provided by this module
		$this->rights = array();
		$r = 0;
		// Add here entries to declare new permissions
		/* BEGIN MODULEBUILDER PERMISSIONS */
		$this->rights[$r][0] = $this->numero + $r; // Permission id (must not be already used)
		$this->rights[$r][1] = 'Read objects of Kimtech'; // Permission label
		$this->rights[$r][4] = 'kimtech'; // In php code, permission will be checked by test if ($user->rights->kimtech->level1->level2)
		$this->rights[$r][5] = 'read'; // In php code, permission will be checked by test if ($user->rights->kimtech->level1->level2)
		$r++;
		$this->rights[$r][0] = $this->numero + $r; // Permission id (must not be already used)
		$this->rights[$r][1] = 'Create/Update objects of Kimtech'; // Permission label
		$this->rights[$r][4] = 'kimtech'; // In php code, permission will be checked by test if ($user->rights->kimtech->level1->level2)
		$this->rights[$r][5] = 'write'; // In php code, permission will be checked by test if ($user->rights->kimtech->level1->level2)
		$r++;
		$this->rights[$r][0] = $this->numero + $r; // Permission id (must not be already used)
		$this->rights[$r][1] = 'Delete objects of Kimtech'; // Permission label
		$this->rights[$r][4] = 'kimtech'; // In php code, permission will be checked by test if ($user->rights->kimtech->level1->level2)
		$this->rights[$r][5] = 'delete'; // In php code, permission will be checked by test if ($user->rights->kimtech->level1->level2)
		$r++;
		/* END MODULEBUILDER PERMISSIONS */

		// Main menu entries to add
		$this->menu = array();
		$r = 0;
		// Add here entries to declare new menus
		/* BEGIN MODULEBUILDER TOPMENU */
		// $this->menu[$r++] = array(
		// 	'fk_menu'=>'', // '' if this is a top menu. For left menu, use 'fk_mainmenu=xxx' or 'fk_mainmenu=xxx,fk_leftmenu=yyy' where xxx is mainmenucode and yyy is a leftmenucode
		// 	'type'=>'top', // This is a Top menu entry
		// 	'titre'=>'ModuleKimtechName',
		// 	'mainmenu'=>'kimtech',
		// 	'leftmenu'=>'',
		// 	'url'=>'/kimtech/kimtechindex.php',
		// 	'langs'=>'kimtech@kimtech', // Lang file to use (without .lang) by module. File must be in langs/code_CODE/ directory.
		// 	'position'=>1000 + $r,
		// 	'enabled'=>'$conf->kimtech->enabled', // Define condition to show or hide menu entry. Use '$conf->kimtech->enabled' if entry must be visible if module is enabled.
		// 	'perms'=>'1', // Use 'perms'=>'$user->rights->kimtech->kimtech->read' if you want your menu with a permission rules
		// 	'target'=>'',
		// 	'user'=>2, // 0=Menu for internal users, 1=external users, 2=both
		// );
		/* END MODULEBUILDER TOPMENU */
		/* BEGIN MODULEBUILDER LEFTMENU KIMTECH
		$this->menu[$r++]=array(
			'fk_menu'=>'fk_mainmenu=kimtech',      // '' if this is a top menu. For left menu, use 'fk_mainmenu=xxx' or 'fk_mainmenu=xxx,fk_leftmenu=yyy' where xxx is mainmenucode and yyy is a leftmenucode
			'type'=>'left',                          // This is a Top menu entry
			'titre'=>'Kimtech',
			'mainmenu'=>'kimtech',
			'leftmenu'=>'kimtech',
			'url'=>'/kimtech/kimtechindex.php',
			'langs'=>'kimtech@kimtech',	        // Lang file to use (without .lang) by module. File must be in langs/code_CODE/ directory.
			'position'=>1000+$r,
			'enabled'=>'$conf->kimtech->enabled',  // Define condition to show or hide menu entry. Use '$conf->kimtech->enabled' if entry must be visible if module is enabled.
			'perms'=>'$user->rights->kimtech->kimtech->read',			                // Use 'perms'=>'$user->rights->kimtech->level1->level2' if you want your menu with a permission rules
			'target'=>'',
			'user'=>2,				                // 0=Menu for internal users, 1=external users, 2=both
		);
		$this->menu[$r++]=array(
			'fk_menu'=>'fk_mainmenu=kimtech,fk_leftmenu=kimtech',	    // '' if this is a top menu. For left menu, use 'fk_mainmenu=xxx' or 'fk_mainmenu=xxx,fk_leftmenu=yyy' where xxx is mainmenucode and yyy is a leftmenucode
			'type'=>'left',			                // This is a Left menu entry
			'titre'=>'List_Kimtech',
			'mainmenu'=>'kimtech',
			'leftmenu'=>'kimtech_kimtech_list',
			'url'=>'/kimtech/kimtech_list.php',
			'langs'=>'kimtech@kimtech',	        // Lang file to use (without .lang) by module. File must be in langs/code_CODE/ directory.
			'position'=>1000+$r,
			'enabled'=>'$conf->kimtech->enabled',  // Define condition to show or hide menu entry. Use '$conf->kimtech->enabled' if entry must be visible if module is enabled. Use '$leftmenu==\'system\'' to show if leftmenu system is selected.
			'perms'=>'$user->rights->kimtech->kimtech->read',			                // Use 'perms'=>'$user->rights->kimtech->level1->level2' if you want your menu with a permission rules
			'target'=>'',
			'user'=>2,				                // 0=Menu for internal users, 1=external users, 2=both
		);
		$this->menu[$r++]=array(
			'fk_menu'=>'fk_mainmenu=kimtech,fk_leftmenu=kimtech',	    // '' if this is a top menu. For left menu, use 'fk_mainmenu=xxx' or 'fk_mainmenu=xxx,fk_leftmenu=yyy' where xxx is mainmenucode and yyy is a leftmenucode
			'type'=>'left',			                // This is a Left menu entry
			'titre'=>'New_Kimtech',
			'mainmenu'=>'kimtech',
			'leftmenu'=>'kimtech_kimtech_new',
			'url'=>'/kimtech/kimtech_card.php?action=create',
			'langs'=>'kimtech@kimtech',	        // Lang file to use (without .lang) by module. File must be in langs/code_CODE/ directory.
			'position'=>1000+$r,
			'enabled'=>'$conf->kimtech->enabled',  // Define condition to show or hide menu entry. Use '$conf->kimtech->enabled' if entry must be visible if module is enabled. Use '$leftmenu==\'system\'' to show if leftmenu system is selected.
			'perms'=>'$user->rights->kimtech->kimtech->write',			                // Use 'perms'=>'$user->rights->kimtech->level1->level2' if you want your menu with a permission rules
			'target'=>'',
			'user'=>2,				                // 0=Menu for internal users, 1=external users, 2=both
		);
		*/

        $this->menu[$r++]=array(
            // '' if this is a top menu. For left menu, use 'fk_mainmenu=xxx' or 'fk_mainmenu=xxx,fk_leftmenu=yyy' where xxx is mainmenucode and yyy is a leftmenucode
            'fk_menu'=>'fk_mainmenu=companies,fk_leftmenu=thirdparties',
            // This is a Left menu entry
            'type'=>'left',
            'titre'=>'Consultar cliente',
            'mainmenu'=>'companies',
            'leftmenu'=>'thirdparties',
            'url'=>'/kimtech/search.php',
            // Lang file to use (without .lang) by module. File must be in langs/code_CODE/ directory.
            'langs'=>'kimtech@kimtech',
            'position'=>1000+$r,
            // Define condition to show or hide menu entry. Use '$conf->kimtech->enabled' if entry must be visible if module is enabled. Use '$leftmenu==\'system\'' to show if leftmenu system is selected.
            'enabled'=>'$conf->kimtech->enabled',
            // Use 'perms'=>'$user->rights->kimtech->level1->level2' if you want your menu with a permission rules
            'perms'=>'1',
            'target'=>'',
            // 0=Menu for internal users, 1=external users, 2=both
            'user'=>2,
        );
        // $this->menu[$r++]=array(
        //     // '' if this is a top menu. For left menu, use 'fk_mainmenu=xxx' or 'fk_mainmenu=xxx,fk_leftmenu=yyy' where xxx is mainmenucode and yyy is a leftmenucode
        //     'fk_menu'=>'fk_mainmenu=kimtech,fk_leftmenu=kimtech_kimtech',
        //     // This is a Left menu entry
        //     'type'=>'left',
        //     'titre'=>'New Kimtech',
        //     'mainmenu'=>'kimtech',
        //     'leftmenu'=>'kimtech_kimtech',
        //     'url'=>'/kimtech/kimtech_card.php?action=create',
        //     // Lang file to use (without .lang) by module. File must be in langs/code_CODE/ directory.
        //     'langs'=>'kimtech@kimtech',
        //     'position'=>1100+$r,
        //     // Define condition to show or hide menu entry. Use '$conf->kimtech->enabled' if entry must be visible if module is enabled. Use '$leftmenu==\'system\'' to show if leftmenu system is selected.
        //     'enabled'=>'$conf->kimtech->enabled',
        //     // Use 'perms'=>'$user->rights->kimtech->level1->level2' if you want your menu with a permission rules
        //     'perms'=>'1',
        //     'target'=>'',
        //     // 0=Menu for internal users, 1=external users, 2=both
        //     'user'=>2
        // );

		/* END MODULEBUILDER LEFTMENU KIMTECH */
		// Exports profiles provided by this module
		$r = 1;
		/* BEGIN MODULEBUILDER EXPORT KIMTECH */
		/*
		$langs->load("kimtech@kimtech");
		$this->export_code[$r]=$this->rights_class.'_'.$r;
		$this->export_label[$r]='KimtechLines';	// Translation key (used only if key ExportDataset_xxx_z not found)
		$this->export_icon[$r]='kimtech@kimtech';
		// Define $this->export_fields_array, $this->export_TypeFields_array and $this->export_entities_array
		$keyforclass = 'Kimtech'; $keyforclassfile='/kimtech/class/kimtech.class.php'; $keyforelement='kimtech@kimtech';
		include DOL_DOCUMENT_ROOT.'/core/commonfieldsinexport.inc.php';
		//$this->export_fields_array[$r]['t.fieldtoadd']='FieldToAdd'; $this->export_TypeFields_array[$r]['t.fieldtoadd']='Text';
		//unset($this->export_fields_array[$r]['t.fieldtoremove']);
		//$keyforclass = 'KimtechLine'; $keyforclassfile='/kimtech/class/kimtech.class.php'; $keyforelement='kimtechline@kimtech'; $keyforalias='tl';
		//include DOL_DOCUMENT_ROOT.'/core/commonfieldsinexport.inc.php';
		$keyforselect='kimtech'; $keyforaliasextra='extra'; $keyforelement='kimtech@kimtech';
		include DOL_DOCUMENT_ROOT.'/core/extrafieldsinexport.inc.php';
		//$keyforselect='kimtechline'; $keyforaliasextra='extraline'; $keyforelement='kimtechline@kimtech';
		//include DOL_DOCUMENT_ROOT.'/core/extrafieldsinexport.inc.php';
		//$this->export_dependencies_array[$r] = array('kimtechline'=>array('tl.rowid','tl.ref')); // To force to activate one or several fields if we select some fields that need same (like to select a unique key if we ask a field of a child to avoid the DISTINCT to discard them, or for computed field than need several other fields)
		//$this->export_special_array[$r] = array('t.field'=>'...');
		//$this->export_examplevalues_array[$r] = array('t.field'=>'Example');
		//$this->export_help_array[$r] = array('t.field'=>'FieldDescHelp');
		$this->export_sql_start[$r]='SELECT DISTINCT ';
		$this->export_sql_end[$r]  =' FROM '.MAIN_DB_PREFIX.'kimtech as t';
		//$this->export_sql_end[$r]  =' LEFT JOIN '.MAIN_DB_PREFIX.'kimtech_line as tl ON tl.fk_kimtech = t.rowid';
		$this->export_sql_end[$r] .=' WHERE 1 = 1';
		$this->export_sql_end[$r] .=' AND t.entity IN ('.getEntity('kimtech').')';
		$r++; */
		/* END MODULEBUILDER EXPORT KIMTECH */

		// Imports profiles provided by this module
		$r = 1;
		/* BEGIN MODULEBUILDER IMPORT KIMTECH */
		/*
		 $langs->load("kimtech@kimtech");
		 $this->export_code[$r]=$this->rights_class.'_'.$r;
		 $this->export_label[$r]='KimtechLines';	// Translation key (used only if key ExportDataset_xxx_z not found)
		 $this->export_icon[$r]='kimtech@kimtech';
		 $keyforclass = 'Kimtech'; $keyforclassfile='/kimtech/class/kimtech.class.php'; $keyforelement='kimtech@kimtech';
		 include DOL_DOCUMENT_ROOT.'/core/commonfieldsinexport.inc.php';
		 $keyforselect='kimtech'; $keyforaliasextra='extra'; $keyforelement='kimtech@kimtech';
		 include DOL_DOCUMENT_ROOT.'/core/extrafieldsinexport.inc.php';
		 //$this->export_dependencies_array[$r]=array('mysubobject'=>'ts.rowid', 't.myfield'=>array('t.myfield2','t.myfield3')); // To force to activate one or several fields if we select some fields that need same (like to select a unique key if we ask a field of a child to avoid the DISTINCT to discard them, or for computed field than need several other fields)
		 $this->export_sql_start[$r]='SELECT DISTINCT ';
		 $this->export_sql_end[$r]  =' FROM '.MAIN_DB_PREFIX.'kimtech as t';
		 $this->export_sql_end[$r] .=' WHERE 1 = 1';
		 $this->export_sql_end[$r] .=' AND t.entity IN ('.getEntity('kimtech').')';
		 $r++; */
		/* END MODULEBUILDER IMPORT KIMTECH */
	}

	/**
	 *  Function called when module is enabled.
	 *  The init function add constants, boxes, permissions and menus (defined in constructor) into Dolibarr database.
	 *  It also creates data directories
	 *
	 *  @param      string  $options    Options when enabling module ('', 'noboxes')
	 *  @return     int             	1 if OK, 0 if KO
	 */
	public function init($options = '')
	{
		global $conf, $langs;

		$result = $this->_load_tables('/kimtech/sql/');
		if ($result < 0) return -1; // Do not activate module if error 'not allowed' returned when loading module SQL queries (the _load_table run sql with run_sql with the error allowed parameter set to 'default')

		// Create extrafields during init
		include_once DOL_DOCUMENT_ROOT.'/core/class/extrafields.class.php';
		$extrafields = new ExtraFields($this->db);
	   	//$tabl =array();

		$extrafields->addExtraField('unit', "Unidades Solicitadas", 'double', 202,  "24,2", 'propaldet',   0, 0, '', '', 1, '', 0, 0, '', '', 'kimtech@kimtech', '$conf->kimtech->enabled');

		$fournisseur =array();
	    $tabla0['societe:nom: rowid :: fournisseur=1'] = '';
    	$fournisseur['options']= $tabla0;

     	$extrafields->addExtraField('fournisseur', "Proveedor", 'sellist', 1,  "", 'propaldet',   0, 0, '', $fournisseur, 1, '', 1, 0, '', '', 'kimtech@kimtech', '$conf->kimtech->enabled');


		$par =array();
	    $tabla1['kimtech_marca:label:label::active=1'] = '';
    	$par['options']= $tabla1;

     	$extrafields->addExtraField('Marca', "Marca", 'sellist', 1,  "", 'product',   0, 1, '', $par, 1, '', 1, 0, '', '', 'kimtech@kimtech', '$conf->kimtech->enabled');


		 $param9 =array();
		$tabla9["document_model:nom: nom::type= 'propal'"] = '';
		$param9['options']= $tabla9;

		$extrafields->addExtraField('pdf_model', "Empresa", 'sellist', 1,  "", 'societe',   0, 0, '', $param9, 1, '', 1, 0, '', '', 'kimtech@kimtech', '$conf->kimtech->enabled');

		$param8 =array();
	 	$tabla8["societe_commerciaux:fk_user: fk_user::fk_soc=($SEL$ fk_soc from llx_propal where rowid = $ID$)"] = '';
		$tabla8["societe_commerciaux as sc left join llx_user as u ON sc.fk_user = u.rowid:CONCAT(firstname,' ',lastname): u.rowid::statut=1 AND sc.fk_soc=($SEL$ fk_soc from llx_propal where rowid=$ID$)"] = '';
		$param8['options']= $tabla8;

		$extrafields->addExtraField('getsales', "Vendedor", 'sellist', 1,  "", 'propal',   0, 0, '', $param8, 1, '', 1, 0, '', '', 'kimtech@kimtech', '$conf->kimtech->enabled');

		$param7 =array();
		$tabla7["document_model:nom: nom::type= 'propal'"] = '';
		$param7['options']= $tabla7;

		$extrafields->addExtraField('pdf_model', "Empresa", 'sellist', 1,  "", 'propal',   0, 0, '', $param7, 1, '', 1, 0, '', '', 'kimtech@kimtech', '$conf->kimtech->enabled');

		$param6 =array();
		$tabla6["document_model:nom: nom::type= 'order'"] = '';
		$param6['options']= $tabla6;

		$extrafields->addExtraField('pdf_model', "Empresa", 'sellist', 1,  "", 'commande',   0, 0, '', $param6, 1, '', 1, 0, '', '', 'kimtech@kimtech', '$conf->kimtech->enabled');

		// Permissions
		$this->remove($options);

		$sql = array();

		//diseño pdf para factura
		$srcpro1=DOL_DOCUMENT_ROOT.'/custom/kimtech/file/formatos/propale/pdf_kimtech.modules.php';

		$destpro1=DOL_DOCUMENT_ROOT.'/core/modules/propale/doc/pdf_kimtech.modules.php';

		if (file_exists($srcpro1) && ! file_exists($destpro1))
		{
			require_once DOL_DOCUMENT_ROOT.'/core/lib/files.lib.php';
			dol_mkdir($dirodt);
			$result=dol_copy($srcpro1, $destpro1, 0, 0);
			if ($result < 0)
			{
				$langs->load("errors");
				$this->error=$langs->trans('ErrorFailToCopyFile', $srcpro1, $destpro1);
				return 0;
			}

		}

		//diseño pdf para factura
		$srcpro2=DOL_DOCUMENT_ROOT.'/custom/kimtech/file/formatos/propale/pdf_tecnimport.modules.php';

		$destpro2=DOL_DOCUMENT_ROOT.'/core/modules/propale/doc/pdf_tecnimport.modules.php';

		if (file_exists($srcpro2) && ! file_exists($destpro2))
		{
			require_once DOL_DOCUMENT_ROOT.'/core/lib/files.lib.php';
			dol_mkdir($dirodt);
			$result=dol_copy($srcpro2, $destpro2, 0, 0);
			if ($result < 0)
			{
				$langs->load("errors");
				$this->error=$langs->trans('ErrorFailToCopyFile', $srcpro2, $destpro2);
				return 0;
			}

		}

		return $this->_init($sql, $options);
	}

	/**
	 *  Function called when module is disabled.
	 *  Remove from database constants, boxes and permissions from Dolibarr database.
	 *  Data directories are not deleted
	 *
	 *  @param      string	$options    Options when enabling module ('', 'noboxes')
	 *  @return     int                 1 if OK, 0 if KO
	 */
	public function remove($options = '')
	{
		$sql = array();
		return $this->_remove($sql, $options);
	}
}
