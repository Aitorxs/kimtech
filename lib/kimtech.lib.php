<?php
/* Copyright (C) 2021 SuperAdmin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file    kimtech/lib/kimtech.lib.php
 * \ingroup kimtech
 * \brief   Library files with common functions for Kimtech
 */

/**
 * Prepare admin pages header
 *
 * @return array
 */
function kimtechAdminPrepareHead()
{
	global $langs, $conf;

	$langs->load("kimtech@kimtech");

	$h = 0;
	$head = array();

	$head[$h][0] = dol_buildpath("/kimtech/admin/setup.php", 1);
	$head[$h][1] = $langs->trans("Settings");
	$head[$h][2] = 'settings';
	$h++;


	$head[$h][0] = dol_buildpath("/kimtech/admin/about.php", 1);
	$head[$h][1] = $langs->trans("About");
	$head[$h][2] = 'about';
	$h++;

	$head[$h][0] = dol_buildpath("/kimtech/admin/kimtech_extrafields.php", 1);
  $head[$h][1] = $langs->trans("Campos Adicionales");
  $head[$h][2] = 'attributes';
  $h++;


	complete_head_from_modules($conf, $langs, null, $head, $h, 'kimtech');

	return $head;
}
