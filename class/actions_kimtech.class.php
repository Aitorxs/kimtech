<?php
/* Copyright (C) 2021 SuperAdmin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file    kimtech/class/actions_kimtech.class.php
 * \ingroup kimtech
 * \brief   Example hook overload.
 *
 * Put detailed description here.
 */
 dol_include_once( '/integracionwordpress/api/vendor/autoload.php');

use Automattic\WooCommerce\Client;
use Automattic\WooCommerce\HttpClient\HttpClientException;
 require_once DOL_DOCUMENT_ROOT.'/product/class/product.class.php';
 require_once DOL_DOCUMENT_ROOT.'/comm/propal/class/propal.class.php';
 require_once DOL_DOCUMENT_ROOT.'/commande/class/commande.class.php';
require_once DOL_DOCUMENT_ROOT.'/core/class/html.formfile.class.php';
require_once DOL_DOCUMENT_ROOT."/core/class/html.formcompany.class.php";
require_once DOL_DOCUMENT_ROOT . '/product/class/html.formproduct.class.php';
require_once DOL_DOCUMENT_ROOT.'/core/lib/ws.lib.php';

 dol_include_once('/kimtech/class/kimtech.class.php');
/**
 * Class ActionsKimtech
 */
class ActionsKimtech
{
	/**
	 * @var DoliDB Database handler.
	 */
	public $db;

	/**
	 * @var string Error code (or message)
	 */
	public $error = '';

	/**
	 * @var array Errors
	 */
	public $errors = array();


	/**
	 * @var array Hook results. Propagated to $hookmanager->resArray for later reuse
	 */
	public $results = array();

	/**
	 * @var string String displayed by executeHook() immediately after return
	 */
	public $resprints;


	/**
	 * Constructor
	 *
	 *  @param		DoliDB		$db      Database handler
	 */
	public function __construct($db)
	{
		$this->db = $db;
    $kimtech =  new kimtech($db);
    $formfile = new FormFile($db);
	}


	/**
	 * Execute action
	 *
	 * @param	array			$parameters		Array of parameters
	 * @param	CommonObject    $object         The object to process (an invoice if you are in invoice module, a propale in propale's module, etc...)
	 * @param	string			$action      	'add', 'update', 'view'
	 * @return	int         					<0 if KO,
	 *                           				=0 if OK but we want to process standard actions too,
	 *                            				>0 if OK and we want to replace standard actions.
	 */
	public function getNomUrl($parameters, &$object, &$action)
	{
		global $db, $langs, $conf, $user;
		$this->resprints = '';
		return 0;
	}

	/**
	 * Overloading the doActions function : replacing the parent's function with the one below
	 *
	 * @param   array           $parameters     Hook metadatas (context, etc...)
	 * @param   CommonObject    $object         The object to process (an invoice if you are in invoice module, a propale in propale's module, etc...)
	 * @param   string          $action         Current action (if set). Generally create or edit or null
	 * @param   HookManager     $hookmanager    Hook manager propagated to allow calling another hook
	 * @return  int                             < 0 on error, 0 on success, 1 to replace standard code
	 */
   public function formObjectOptions($parameters, &$object, &$action, $hookmanager)
     {
       global $conf, $user, $langs;

   		$error = 0; // Error counter

   		if (in_array('productcard', explode(':', $parameters['context'])))
   		{

        //ocultar etiqueta de producto
         print '
         <script type="text/javascript">

            $("input[name=\'label\']").parent().parent().css("display","none");
          
          $(".refidno").hide();




            $(document).ready(function () {
              $("#desc").keyup(function () {
              var value = $(this).val();
              $("input[name=\'label\']").val(value);            
            });
        });

         </script>';


   			if($action == 'create'){

          //posicion de campo extra marca Machfree
          $_POST['label']='-';
   				print '

   				<script type="text/javascript">

   				$(document).ready(function(){

        	  $("#categories").parent().parent().prependTo($("#ref").parent().parent().parent());
   					$("#options_Marca").parent().parent().prependTo($("#ref").parent().parent().parent());
    
            
   			   });

   				</script>
   				';

            

   				//$this->resprints = $consultaruc;
   				return 0; // or return 1 to replace standard code

   			}
   	}

    if (in_array('propalcard', explode(':', $parameters['context'])))
    {



  }

  }

  public function formAddObjectLine($parameters, &$object, &$action, $hookmanager)
  {

    global $conf, $user, $langs,$db;
    $langs->load('kimtech@kimtech');

    dol_include_once('/kimtech/class/kimtech.class.php');


    $kimtech = new kimtech($db);
    $userstatic = new User($db);
    $form = new Form($db);
    $product = new Product($db);
    $propal     = new Propal($db);
    $propaldet  = new PropaleLigne($db);

    switch ($object->element) {
        case 'propal':
            $Did = 'id';
            $id  = GETPOST('id', 'int');
            break;
        case 'facture':
            $Did = 'facid';
             $id  = GETPOST('facid', 'int');
            break;
        default:
           $Did = 'id';
            $id  = GETPOST('id', 'int');
            break;
    }
    $ref = GETPOST('ref', 'alpha');

    $error = 0; // Error counter

    /* print_r($parameters); print_r($object); echo "action: " . $action; */
    if (in_array('propalcard', explode(':', $parameters['context'])))
    {

      if($action == 'confirm_validate' && $confirm == 'yes'){
        print '
               <script type="text/javascript">
                      $(document).ready(function(){
                         tablelines.setAttribute("style","display:none");

                       });
               </script>
               ';
      }


      if($action == 'addPr')
      {

        $qty = GETPOST('qty2','int');
        $price_ht = GETPOST('price_ht2','double');
        $tva_tx = GETPOST('tva_tx2','int');
        $fk_produc = GETPOST('id_pro2','int');
        $tva =GETPOST('tva','int');
        $marca =GETPOST('marca','alpha');
        $kimtech->load_cache_marca2($marca);
        $kimtech2 = new kimtech($db);
        $categorie=GETPOST('categoria','alpha');
        $kimtech2->load_cache_categorie2($categorie);
        $desc = GETPOST('search_idprod2','alpha').'-'.GETPOST('dp_desc','alpha').'-'.$kimtech->label.'-'.$kimtech2->label;
        $cod = GETPOST('txt_id','alpha');
        $fk_unit = GETPOST('units','int');
        $remise_percent= GETPOST('remise_percent1','alpha');
        

        if($fk_produc != null )
        {

          $product->fetch($fk_produc);
          $product_price =   $product->price_ttc;
          $product_unit =   $product->fk_unit;
          $vatrate=$product->tva_tx;
          $qty = GETPOST('qty2','int');
          $localtax1_tx=0;
          $localtax2_tx=0;
          
          $info_bits=0;
          $product_type=0;
          $rang= -1;
          $special_code=0;
          $fk_parent_line='';
          $fk_fournprice=0;
          $label ='';
          $desc = GETPOST('dp_desc','alpha')?GETPOST('dp_desc','alpha'):  $product->description;
          $date_start='';
          $date_end='';
          $array_options=0;
          $origin ='';
          $origin_id = $object->id;
          $pa_ht=GETPOST('buying_price1','alpha')? GETPOST('buying_price1','alpha'):GETPOST('fournprice_predef1','alpha') ;
          $fk_remise_except=0;

          $res = $object->addline($desc,$price_ht,$qty,$vatrate,$localtax1_tx,$localtax2_tx,$fk_produc,$remise_percent,'HT',$price_ht,$info_bits,$product_type,$rang,$special_code,$fk_parent_line,$fk_fournprice,$pa_ht,$label,$date_start,$date_end,$array_options,$product_unit,$origin,$origin_id,$pu_ht_devise,$fk_remise_except);

        }else
        {

          if (empty(GETPOST('search_idprod2','alpha'))) {
            setEventMessages($langs->trans('Código de producto requerido'), null, 'errors');
             $url='"'.$_SERVER["PHP_SELF"].'?id='.$object->id;
            echo '<meta http-equiv=refresh content="1; '.$url.'">';
            return -1;
          }

          if(GETPOST('dp_desc','alpha')) $dp_desc = ' - '.GETPOST('dp_desc','alpha');
          $desc= $kimtech->label.' - '.$kimtech2->label.' - '. GETPOST('search_idprod2','alpha').$dp_desc;
          $pu_ht=GETPOST('price_ht2','double');
          $qty=GETPOST('qty2','int');
          $vatrate=GETPOST('tva_tx2','int');
          $localtax1_tx=0;
          $localtax2_tx=0;
          $fk_product='';
          $pu_ttc=GETPOST('price_ht2','double');
          $info_bits=0;
          $product_type=0;
          $rang= -1;
          $special_code=0;
          $fk_parent_line='';
          $fk_fournprice=0;
          $label = '';
          $date_start='';
          $date_end='';
          $array_options=0;
          $fk_unit= $fk_unit;
          $pa_ht=GETPOST('buying_price1','alpha')? GETPOST('buying_price1','alpha'): 0;
          $origin ='';
          $origin_id = $object->id;
          $fk_remise_except=0;
          
          $res = $object->addline($desc,$pu_ht,$qty,$vatrate,$localtax1_tx,$localtax2_tx,$fk_product,$remise_percent,'HT',$pu_ttc,$info_bits,$product_type,$rang,$special_code,$fk_parent_line,$fk_fournprice,$pa_ht,$label,$date_start,$date_end,$array_options,$fk_unit,$origin,$origin_id,$pu_ht_devise,$fk_remise_except);     
          
        }

        $s_unit = GETPOST('s_unit','int');
        $fournisseur =GETPOST('options_fournisseur','int');
        $kimtech->tms = date("Y-m-d");
        $kimtech->fk_object = $res;
        $kimtech->unit = $s_unit;
        $kimtech->fournisseur = $fournisseur;
        $kimtech->createpropaldet_extrafields($user);


          // print '
          //   <script type="text/javascript">
          //     $(document).ready(function(){
          //        tablelines.setAttribute("style","display:none");

          //      });
          //   </script>';

        if ($res >= 0) {
            $url='"'.$_SERVER["PHP_SELF"].'?id='.$object->id;
            echo '<meta http-equiv=refresh content="0.1; '.$url.'">';
            die;
          } else {
            dol_print_error($db);
          }

      }

      global $forceall, $forcetoshowtitlelines, $senderissupplier, $inputalsopricewithtax;


      if($conf->global->MAIN_MODULE_KIMTECH)
      {
         print ' <script type="text/javascript">
                $(document).ready(function(){
                  tablelines.setAttribute("style","display:none");
                 });
                 </script>';


        print '<div class="div-table-responsive-no-min">';
        print '<table id  class="noborder noshadow" width="100%">';

        // title lines
        require_once DOL_DOCUMENT_ROOT.'/custom/kimtech/core/tpl/objectline_title.tpl.php';

        // list lines products
        $sql = 'SELECT p.rowid as id, p.rowid,p.fk_product,pr.unit, pr.fournisseur,p.description,p.fk_parent_line,p.product_type,p.tva_tx,p.total_tva,p.fk_unit,p.subprice,p.multicurrency_subprice,p.remise_percent,p.buy_price_ht as pa_ht,p.total_ht,p.total_ttc,p.multicurrency_total_ht,p.multicurrency_total_ttc,p.qty,ef.rowid,ef.ref,ef.label';
        $sql .= ' FROM '.MAIN_DB_PREFIX.'propaldet as p';
        $sql .= " LEFT JOIN ".MAIN_DB_PREFIX."propaldet_extrafields as pr on p.rowid = pr.fk_object";
        $sql .= " LEFT JOIN ".MAIN_DB_PREFIX."product as ef on  ef.rowid =p.fk_product";
        $sql .= ' WHERE fk_propal = '.$object->id.' order by p.rang';

        $result = $db->query($sql);
        if ($result) 
        {
          $num = $db->num_rows($result);
          $i = 0;
          $totalarray = array();

          if ($num > 0 ) 
          {
            while ($i < $num) 
            {
              $line = $db->fetch_object($result);
              $tpl = DOL_DOCUMENT_ROOT.'/custom/kimtech/core/tpl/objectline_view.tpl.php';
              $res = @include $tpl;

              $i++;
            }
          }
          $db->free($result);
        } else {
       
          dol_print_error($db);
        }

        //add product
        print '<form action="'.$_SERVER["PHP_SELF"].'" name="formsoc" method="post">
              <input type="hidden" name="token" value="' . newToken().'">
              <input type="hidden" name="action" value="addPr">
              <input type="hidden" name="mode" value="">
              <input type="hidden" name="id" value="' . $object->id.'">';

        print '<table id="tableline2"  class="noborder noshadow" width="100%">';


        print '<tr>';
        
        print '<td colspan="4">';
        //Marca
        print $langs->trans('Marca').': ';
        $kimtech->load_cache_marca();
        print '<select class="flat" onchange="load(this.value)" id="marca" name="marca">';
        foreach($kimtech->cache_marca as $id => $arraymarca)
        {
          if ($selected == $id)
          {
            print '<option value="'.$id.'" selected="selected">';
          }
          else
          {
            print '<option value="'.$id.'">';
          }
          print $arraymarca['label'];
          print '</option>';
        }
        print '</select>';
        print '&nbsp;&nbsp;&nbsp;&nbsp;';

        //Category
        print $langs->trans('Categoryk').': ';
        $kimtech->load_cache_categorie();
        print '<select class="flat" id ="categoria"  name="categoria">';
        
        foreach($kimtech->cache_categorie as $id => $arraycategorie)
        {
          if ($selected == $id)
          {
            print '<option value="'.$id.'" selected="selected">';
          }
          else
          {
            print '<option value="'.$id.'">';
          }
          print $arraycategorie['label'];
          print '</option>';
        }
        print '</select>';
        print '</td>';

        print  $kimtech->get_fournisseur();

        print '</tr>';

        print '<tr>';
        print '<td >';
              //code product
        print $langs->trans('Code').': ';
        $kimtech->select_produits(GETPOST('idprod2'), 'idprod2', $filtertype, $conf->product->limit_size, $buyer->price_level, 1, 2, '', 1, array(), $buyer->id, '1', 0, 'maxwidth500', 0, '', GETPOST('combinations', 'array'));

        ?>
        <script>
          $(document).ready(function(){
            // On first focus on a select2 combo, auto open the menu (this allow to use the keyboard only)
            $(document).on('focus', '.select2-selection.select2-selection--single', function (e) {
              console.log('focus on a select2');
              if ($(this).attr('aria-labelledby') == 'select2-idprod2-container')
              {
                console.log('open combo');
                $('#idprod2').select2('open');
              }
            });
          });
        </script>
        <?php

        print '<br>';


        require_once DOL_DOCUMENT_ROOT.'/core/class/doleditor.class.php';
        $nbrows = ROWS_2;
        $enabled = (!empty($conf->global->FCKEDITOR_ENABLE_DETAILS) ? $conf->global->FCKEDITOR_ENABLE_DETAILS : 0);
        if (!empty($conf->global->MAIN_INPUT_DESC_HEIGHT)) $nbrows = $conf->global->MAIN_INPUT_DESC_HEIGHT;
        $toolbarname = 'dolibarr_details';
        if (!empty($conf->global->FCKEDITOR_ENABLE_DETAILS_FULL)) $toolbarname = 'dolibarr_notes';
        // $doleditor = new DolEditor('dp_desc', GETPOST('dp_desc', 'restricthtml'), '', (empty($conf->global->MAIN_DOLEDITOR_HEIGHT) ? 100 : $conf->global->MAIN_DOLEDITOR_HEIGHT), $toolbarname, '', false, true, $enabled, $nbrows, '90%');
        // $doleditor->Create();

        $doleditor = new DolEditor('dp_desc', GETPOST('dp_desc', 'restricthtml'), '', (empty($conf->global->MAIN_DOLEDITOR_HEIGHT) ? 100 : $conf->global->MAIN_DOLEDITOR_HEIGHT), $toolbarname, '', false, true, $enabled, $nbrows, '80%');
        $doleditor->Create();


        print '</td>';

        //VAT
        print '<td style="display:none">';
        $coldisplay++;
        if ($seller->tva_assuj == "0") print '<input type="hidden" name="tva_tx2" id="tva_tx2" value="">'.vatrate(0, true);
        else print $form->load_tva('tva_tx2', 18, 'propalcard','propalcard', 0, 0, '', false, 1);
        print '</td>';
        $coldisplay++;
        ?>
          <input type="text" size="5" style="visibility:collapse;" name="txt_id" id="txt_id" class="flat right" value="<?php echo (GETPOSTISSET("txt_id") ? GETPOST("txt_id", 'alpha', 2) : ''); ?>">
       
        <script>
        const formulario = document.querySelector("input#search_idprod2");
        const filtrar = ()=>{
        console.log(formulario.value);
        $(document).ready(function () {
        $("input#search_idprod2").keyup(function () {
        var value = $(this).val();
        $("#txt_id").val(value);
        });
        });
        }
        formulario.addEventListener('keyup',filtrar);

        </script>
        <?php

           
        print '<td><?php $coldisplay++; ?>
          <input type="text" size="5" name="price_ht2" id="price_ht2" class="flat right" placeholder="P.Venta" value="'.(GETPOSTISSET("price_ht2") ? GETPOST("price_ht2", "alpha", 2) : "").'">
        </td>';
  
        print '<input type="text" size="5"  style="display:none;"  name="tva" id="tva" class="flat right" value="'.(GETPOSTISSET("tva") ? GETPOST("tva", "alpha", 2) : "").'">';

        print '<input type="text" size="5" name="id_pro2" style="display:none" id="id_pro2" class="flat right" value="'.(GETPOSTISSET("id_pro2") ? GETPOST("id_pro2", "alpha", 2) : "").'">';

        
        print '<td ><input type="text" size="2" name="qty2" id="qty2" class="flat right" value="'.(GETPOSTISSET("qty2") ? GETPOST("qty2", 'alpha', 2) : 1).'">
         </td>';
        
        //qty required
        print '<td ><?php $coldisplay++; ?>
         <input type="text" size="5" name="s_unit" id="s_unit"  placeholder="Ca.Req" class="flat right" value="'.(GETPOSTISSET("s_unit") ? GETPOST("s_unit", 'alpha', 2) : "").'"></td>';

        // Unit type
        print '<td>';
        print $form->selectUnits(empty($line->fk_unit) ? $conf->global->PRODUCT_USE_UNITS : $line->fk_unit, "units");
        print '</td>';

        print '<td style="display: none;" class=" nowrap linecoldiscount right"><input type="text" size="1" name="remise_percent1" id="remise_percent1" class="flat right" value="'.(GETPOSTISSET("remise_percent1") ? GETPOST("remise_percent1", "alpha", 2) : 0).'"><span class="hideonsmartphone">%</span></td> ';
        // if (!empty($usemargins)) {
          if (!empty($user->rights->margins->creer)) {
            $coldisplay++;
            ?>
            <td >

              <?php
             
              ?>
              <!-- For predef product -->
              <?php if (!empty($conf->product->enabled) || !empty($conf->service->enabled)) { ?>
                <select id="fournprice_predef1" name="fournprice_predef1"  class="flat minwidth75imp maxwidth20" style="display: none;"></select>
              <?php } ?>
              <!-- For free product -->
              <input type="text" id="buying_price1" name="buying_price1" placeholder="P.Compra" class="flat maxwidth75 maxwidth20 right" value="<?php echo (GETPOSTISSET("buying_price1") ? GETPOST("buying_price1", 'alpha', 2) : ''); ?>">
            </td>
            <?php
            if (!empty($conf->global->DISPLAY_MARGIN_RATES))
            {
              echo '<td ><input class="flat right" placeholder="Margen" type="text" size="2" id="np_marginRate1" name="np_marginRate1" value="'.(GETPOSTISSET("np_marginRate1") ? GETPOST("np_marginRate1", 'alpha', 2) : '').'"><span class="np_marginRate1 hideonsmartphone">%</span></td>';
              $coldisplay++;
            }
            if (!empty($conf->global->DISPLAY_MARK_RATES))
            {
              echo '<td ><input class="flat right" placeholder="Margen" type="text" size="2" id="np_markRate" name="np_markRate" value="'.(GETPOSTISSET("np_markRate") ? GETPOST("np_markRate", 'alpha', 2) : '').'"><span class="np_markRate hideonsmartphone">%</span></td>';
              $coldisplay++;
            }
          }
        // }
        //button add        
        print '<td>';
        print '<input type="submit" class="button" name="bouton" value="'.$langs->trans('Add').'">';
        print '</td>';

        print '</tr>';
        
        

       print '</table>';

        print '</form>';

      print "<script>\n";
      if (!empty($usemargins) && $user->rights->margins->creer)
      {
        ?>
        /* Some js test when we click on button "Add" */
        jQuery(document).ready(function() {
        <?php
        if (!empty($conf->global->DISPLAY_MARGIN_RATES)) { ?>
        $("input[name='np_marginRate1']:first").blur(function(e) {
        return checkFreeLine(e, "np_marginRate1");
        });
        <?php
        }
        if (!empty($conf->global->DISPLAY_MARK_RATES)) { ?>
        $("input[name='np_markRate']:first").blur(function(e) {
        return checkFreeLine(e, "np_markRate");
        });
        <?php
        }
        ?>
        });


        /* TODO This does not work for number with thousand separator that is , */
        function checkFreeLine(e, npRate)
                {
                  var buying_price1 = $("input[name='buying_price1']:first");
                  var remise = $("input[name='remise_percent1']:first");

                  var rate = $("input[name='"+npRate+"']:first");
                  if (rate.val() == '')
                    return true;

                  if (! $.isNumeric(rate.val().replace(',','.')))
                  {
                    alert('<?php echo dol_escape_js($langs->trans("rateMustBeNumeric")); ?>');
                    e.stopPropagation();
                    setTimeout(function () { rate.focus() }, 50);
                    return false;
                  }
                  if (npRate == "np_markRate" && rate.val() >= 100)
                  {
                    alert('<?php echo dol_escape_js($langs->trans("markRateShouldBeLesserThan100")); ?>');
                    e.stopPropagation();
                    setTimeout(function () { rate.focus() }, 50);
                    return false;
                  }

                  var price = 0;
                  remisejs=price2numjs(remise.val());

                  if (remisejs != 100)  // If a discount not 100 or no discount
                  {
                    if (remisejs == '') remisejs=0;

                    bpjs=price2numjs(buying_price1.val());
                    ratejs=price2numjs(rate.val());

                    if (npRate == "np_marginRate1")
                      price = ((bpjs * (ratejs / 100))+bpjs );
                    else if (npRate == "np_markRate")
                      price = ((bpjs * (ratejs / 100))+bpjs );
                  }

                  $("input[name='price_ht2']:first").val(price); // TODO Must use a function like php price to have here a formated value

                  return true;
                }

        <?php
      }
      ?>

        /* JQuery for product free or predefined select */
        jQuery(document).ready(function() {
          jQuery("#price_ht").keyup(function(event) {
            // console.log(event.which);    // discard event tag and arrows
            if (event.which != 9 && (event.which < 37 ||event.which > 40) && jQuery("#price_ht").val() != '') {
            jQuery("#price_ttc").val('');
            jQuery("#multicurrency_subprice").val('');
          }
        });
        jQuery("#price_ttc").keyup(function(event) {
          // console.log(event.which);    // discard event tag and arrows
          if (event.which != 9 && (event.which < 37 || event.which > 40) && jQuery("#price_ttc").val() != '') {
            jQuery("#price_ht").val('');
            jQuery("#multicurrency_subprice").val('');
          }
        });
        jQuery("#multicurrency_subprice").keyup(function(event) {
          // console.log(event.which);    // discard event tag and arrows
          if (event.which != 9 && (event.which < 37 || event.which > 40) && jQuery("#price_ttc").val() != '') {
            jQuery("#price_ht").val('');
            jQuery("#price_ttc").val('');
          }
        });

        $("#prod_entry_mode_free").on( "click", function() {
          setforfree();
        });
        $("#select_type").change(function()
        {
          setforfree();
          if (jQuery('#select_type').val() >= 0)
          {
            /* focus work on a standard textarea but not if field was replaced with CKEDITOR */
            jQuery('#dp_desc').focus();
            /* focus if CKEDITOR */
            if (typeof CKEDITOR == "object" && typeof CKEDITOR.instances != "undefined")
            {
              var editor = CKEDITOR.instances['dp_desc'];
              if (editor) { editor.focus(); }
            }
          }
          console.log("Hide/show date according to product type");
          if (jQuery('#select_type').val() == '0')
          {
            jQuery('#trlinefordates').hide();
            jQuery('.divlinefordates').hide();
          }
          else
          {
            jQuery('#trlinefordates').show();
            jQuery('.divlinefordates').show();
          }
        });

        $("#prod_entry_mode_predef").on( "click", function() {
          console.log("click prod_entry_mode_predef");
          setforpredef();
          jQuery('#trlinefordates').show();
        });

        <?php
        if (!$freelines) { ?>
          $("#prod_entry_mode_predef").click();
          <?php
        }
        ?>

        /* When changing predefined product, we reload list of supplier prices required for margin combo */
        $("#idprod2, #idprodfournprice2").change(function()
        {
          console.log("Call method change() after change on #idprod or #idprodfournprice (senderissupplier=<?php echo $senderissupplier; ?>). this.val = "+$(this).val());

          setforpredef();   // TODO Keep vat combo visible and set it to first entry into list that match result of get_default_tva

          jQuery('#trlinefordates').show();

          <?php
          if (empty($conf->global->MAIN_DISABLE_EDIT_PREDEF_PRICEHT) && empty($senderissupplier))
          {
            ?>
            var pbq = parseInt($('option:selected', this).attr('data-pbq'));  /* If product was selected with a HTML select */
            if (isNaN(pbq)) { pbq = jQuery('#idprod').attr('data-pbq'); }     /* If product was selected with a HTML input with autocomplete */
            //console.log(pbq);

            if ((jQuery('#idprod2').val() > 0 || jQuery('#idprodfournprice2').val()) && ! isNaN(pbq) && pbq > 0)
            {
              console.log("We are in a price per qty context, we do not call ajax/product, init of fields is done few lines later");
            } else {
              <?php if (!empty($conf->global->PRODUIT_CUSTOMER_PRICES_BY_QTY) || !empty($conf->global->PRODUIT_CUSTOMER_PRICES_BY_QTY_MULTIPRICES)) { ?>
                if (isNaN(pbq)) { console.log("We use experimental option PRODUIT_CUSTOMER_PRICES_BY_QTY or PRODUIT_CUSTOMER_PRICES_BY_QTY but we could not get the id of pbq from product combo list, so load of price may be 0 if product has differet prices"); }
              <?php } ?>
              // Get the HT price for the product and display it
              console.log("Load unit price without tax and set it into #price_ht for product id="+$(this).val()+" socid=<?php print $object->socid; ?>");
              $.post('<?php echo DOL_URL_ROOT; ?>/product/ajax/products.php?action=fetch',
                { 'id': $(this).val(), 'socid': <?php print $object->socid; ?> },
                function(data) {
                    var idprod2 =  jQuery('#idprod2').val();
                    console.log("Load unit price end, we got value "+data.price_ht);
                    console.log("Load unit price end, we got value "+data.tva_tx);
                     tva_tx2.setAttribute("style","display:none");
                    jQuery("#id_pro2").val(idprod2);
                    jQuery("#price_ht2").val(data.price_ht);
                    jQuery("#tva").val(data.tva_tx);
                     jQuery("#np_marginRate1, #np_markRate, .np_marginRate1, .np_markRate, #title_units").hide();
                },
                'json'
              );
            }
            <?php
          }

          if ($user->rights->margins->creer)
          {
            $langs->load('stocks');
            ?>

            /* Code for margin */
            $("#fournprice_predef1").find("option").remove();
            $("#fournprice_predef1").hide();
            $("#buying_price1").val("").show();

            /* Call post to load content of combo list fournprice_predef */
            var token = '<?php echo currentToken(); ?>';    // For AJAX Call we use old 'token' and not 'newtoken'
            $.post('<?php echo DOL_URL_ROOT; ?>/custom/kimtech/ajax/getSupplierPrices2.php?bestpricefirst=1', { 'idprod': $(this).val(), 'token': token }, function(data) {
              if (data && data.length > 0)
              {
                var options = ''; var defaultkey = ''; var defaultprice = ''; var bestpricefound = 0;

                var bestpriceid = 0; var bestpricevalue = 0;
                var pmppriceid = 0; var pmppricevalue = 0;
                var costpriceid = 0; var costpricevalue = 0;

                /* setup of margin calculation */
                var defaultbuyprice = '<?php
                if (isset($conf->global->MARGIN_TYPE))
                {
                  if ($conf->global->MARGIN_TYPE == '1')   print 'bestsupplierprice';
                  if ($conf->global->MARGIN_TYPE == 'pmp') print 'pmp';
                  if ($conf->global->MARGIN_TYPE == 'costprice') print 'costprice';
                } ?>';
                console.log("we will set the field for margin. defaultbuyprice="+defaultbuyprice);

                var i = 0;
                $(data).each(function() {
                  /* Warning: Lines must be processed in order: best supplier price, then pmpprice line then costprice */
                  if (this.id != 'pmpprice' && this.id != 'costprice')
                  {
                    i++;
                    this.price = parseFloat(this.price); // to fix when this.price >0
                    // If margin is calculated on best supplier price, we set it by defaut (but only if value is not 0)
                    //console.log("id="+this.id+"-price="+this.price+"-"+(this.price > 0));
                    if (bestpricefound == 0 && this.price > 0) { defaultkey = this.id; defaultprice = this.price; bestpriceid = this.id; bestpricevalue = this.price; bestpricefound=1; } // bestpricefound is used to take the first price > 0
                  }
                  if (this.id == 'pmpprice')
                  {
                    // If margin is calculated on PMP, we set it by defaut (but only if value is not 0)
                    console.log("id="+this.id+"-price="+this.price);
                    if ('pmp' == defaultbuyprice || 'costprice' == defaultbuyprice)
                    {
                      if (this.price > 0) {
                        defaultkey = this.id; defaultprice = this.price; pmppriceid = this.id; pmppricevalue = this.price;
                        //console.log("pmppricevalue="+pmppricevalue);
                      }
                    }
                  }
                  if (this.id == 'costprice')
                  {
                    // If margin is calculated on Cost price, we set it by defaut (but only if value is not 0)
                    console.log("id="+this.id+"-price="+this.price+"-pmppricevalue="+pmppricevalue);
                    if ('costprice' == defaultbuyprice)
                    {
                      if (this.price > 0) { defaultkey = this.id; defaultprice = this.price; costpriceid = this.id; costpricevalue = this.price; }
                      else if (pmppricevalue > 0) { defaultkey = 'pmpprice'; defaultprice = pmppricevalue; }
                    }
                  }
                  options += '<option value="'+this.id+'" price="'+this.price+'">'+this.label+'</option>';
                });
                options += '<option value="inputprice" price="'+defaultprice+'"><?php echo $langs->trans("InputPrice"); ?></option>';

                console.log("finally selected defaultkey="+defaultkey+" defaultprice for buying price="+defaultprice);

                $("#fournprice_predef1").html(options).show();
                if (defaultkey != '')
                {
                  $("#fournprice_predef1").val(defaultkey);
                }

                /* At loading, no product are yet selected, so we hide field of buying_price */
                $("#buying_price1").hide();

                /* Define default price at loading */
                var defaultprice = $("#fournprice_predef1").find('option:selected').attr("price");
                $("#buying_price1").val(defaultprice);

                $("#fournprice_predef1").change(function() {
                  console.log("change on fournprice_predef1");
                  /* Hide field buying_price1 according to choice into list (if 'inputprice' or not) */
                  var linevalue=$(this).find('option:selected').val();
                  var pricevalue = $(this).find('option:selected').attr("price");
                  if (linevalue != 'inputprice' && linevalue != 'pmpprice') {
                    $("#buying_price1").val(pricevalue).hide();  /* We set value then hide field */
                  }
                  if (linevalue == 'inputprice') {
                    $('#buying_price1').show();
                  }
                  if (linevalue == 'pmpprice') {
                    $("#buying_price1").val(pricevalue);
                    $('#buying_price1').hide();
                  }
                });
              }
            },
            'json');

            <?php
          }
          ?>

          /* To process customer price per quantity (CUSTOMER_PRICE_PER_QTY works only if combo product is not an ajax after x key pressed) */
          var pbq = parseInt($('option:selected', this).attr('data-pbq'));        // When select is done from HTML select
          if (isNaN(pbq)) { pbq = jQuery('#idprod').attr('data-pbq'); }         // When select is done from HTML input with autocomplete
          var pbqup = parseFloat($('option:selected', this).attr('data-pbqup'));
          if (isNaN(pbqup)) { pbqup = jQuery('#idprod').attr('data-pbqup'); }
          var pbqbase = $('option:selected', this).attr('data-pbqbase');
          if (isNaN(pbqbase)) { pbqbase = jQuery('#idprod').attr('data-pbqbase'); }
          var pbqqty = parseFloat($('option:selected', this).attr('data-pbqqty'));
          if (isNaN(pbqqty)) { pbqqty = jQuery('#idprod').attr('data-pbqqty');  }
          var pbqpercent = parseFloat($('option:selected', this).attr('data-pbqpercent'));
          if (isNaN(pbqpercent)) { pbqpercent = jQuery('#idprod').attr('data-pbqpercent');  }

          if ((jQuery('#idprod').val() > 0 || jQuery('#idprodfournprice').val()) && ! isNaN(pbq) && pbq > 0)
          {
            var pbqupht = pbqup;  /* TODO support of price per qty TTC not yet available */

            console.log("We choose a price by quanty price_by_qty id = "+pbq+" price_by_qty upht = "+pbqupht+" price_by_qty qty = "+pbqqty+" price_by_qty percent = "+pbqpercent);
            jQuery("#pbq").val(pbq);
            jQuery("#price_ht").val(pbqupht);
            if (jQuery("#qty").val() < pbqqty)
            {
              jQuery("#qty").val(pbqqty);
            }
            if (jQuery("#remise_percent").val() < pbqpercent)
            {
              jQuery("#remise_percent").val(pbqpercent);
            }
          }
          else
          {
            jQuery("#pbq").val('');
          }

          /* To set focus */
          if (jQuery('#idprod').val() > 0 || jQuery('#idprodfournprice').val() > 0)
          {
            /* focus work on a standard textarea but not if field was replaced with CKEDITOR */
            jQuery('#dp_desc').focus();
            /* focus if CKEDITOR */
            if (typeof CKEDITOR == "object" && typeof CKEDITOR.instances != "undefined")
            {
              var editor = CKEDITOR.instances['dp_desc'];
              if (editor) { editor.focus(); }
            }
          }
        });

          <?php if (GETPOST('prod_entry_mode') == 'predef') { // When we submit with a predef product and it fails we must start with predef ?>
          setforpredef();
          <?php } ?>
        });

        /* Function to set fields from choice */
        function setforfree() {
          console.log("Call setforfree. We show most fields");
          jQuery("#idprodfournprice").val('0'); // Set cursor on not selected product
          jQuery("#prod_entry_mode_free").prop('checked',true).change();
          jQuery("#prod_entry_mode_predef").prop('checked',false).change();
          jQuery("#search_idprod, #idprod, #search_idprodfournprice, #buying_price1").val('');
          jQuery("#price_ht, #multicurrency_price_ht, #price_ttc, #price_ttc, #fourn_ref, #tva_tx, #buying_price1, #title_fourn_ref, #title_vat, #title_up_ht, #title_up_ht_currency, #title_up_ttc, #title_up_ttc_currency").show();
          jQuery("#np_marginRate, #np_markRate, .np_marginRate, .np_markRate,  #title_units").show();
          jQuery("#fournprice_predef1").hide();
        }
        function setforpredef() {
          console.log("Call setforpredef. We hide some fields and show dates");
          jQuery("#select_type").val(-1);
          jQuery("#prod_entry_mode_free").prop('checked',false).change();
          jQuery("#prod_entry_mode_predef").prop('checked',true).change();
          <?php if (empty($conf->global->MAIN_DISABLE_EDIT_PREDEF_PRICEHT)) { ?>
            jQuery("#price_ht").val('').show();
            jQuery("#multicurrency_price_ht").val('').show();
            jQuery("#title_up_ht, #title_up_ht_currency").show();
          <?php } else { ?>
            jQuery("#price_ht").val('').hide();
            jQuery("#multicurrency_price_ht").val('').hide();
            jQuery("#title_up_ht, #title_up_ht_currency").hide();
          <?php } ?>
          jQuery("#price_ttc, #fourn_ref, #tva_tx, #title_fourn_ref, #title_vat, #title_up_ttc, #title_up_ttc_currency").hide();
          jQuery("#np_marginRate, #np_markRate, .np_marginRate, .np_markRate,  #title_units").hide();
          jQuery("#buying_price1").show();
          jQuery('#trlinefordates, .divlinefordates').show();
        }

      <?php

      print '</script>';

      }

    }

    if (! $error) {
        return 0; // or return 1 to replace standard code
    } else {
        $this->errors[] = 'Error message';
        return -1;
    }
  }

	public function doActions($parameters, &$object, &$action, $hookmanager)
	{
		global $conf, $user, $langs;

		$error = 0; // Error counter

		/* print_r($parameters); print_r($object); echo "action: " . $action; */
		if (in_array($parameters['currentcontext'], array('somecontext1', 'somecontext2')))	    // do something only for the context 'somecontext1' or 'somecontext2'
		{
			// Do what you want here...
			// You can for example call global vars like $fieldstosearchall to overwrite them, or update database depending on $action and $_POST values.
		}


		if (in_array($parameters['currentcontext'], array('propalcard')))	    // do something only for the context 'somecontext1' or 'somecontext2'
		{
			if($action=='create'){

				if(GETPOST('socid')>0){
					$societ = new Societe($this->db);
					$societ->fetch(GETPOST('socid'));
					$conf->global->PROPALE_ADDON_PDF = $societ->array_options['options_pdf_model'];

					$_POST['options_pdf_model']= $societ->array_options['options_pdf_model'];
				}

			}

		}

		if (in_array($parameters['currentcontext'], array('ordercard')))	    // do something only for the context 'somecontext1' or 'somecontext2'
		{
			if($action=='create'){

				if(GETPOST('socid')>0){
					$societ = new Societe($this->db);
					$societ->fetch(GETPOST('socid'));
					$conf->global->COMMANDE_ADDON_PDF = $societ->array_options['options_pdf_model'];

					$_POST['options_pdf_model']= $societ->array_options['options_pdf_model'];
				}

			}

		}


		if (in_array($parameters['currentcontext'], array('propalcard')))	    // do something only for the context 'somecontext1' or 'somecontext2'
		{
			if($action=='confirm_validate'){

				if($object->array_options['options_getsales'] ==0)
				{
					setEventMessages('Debe seleccionar vendedor', '', 'warnings');
					return -1;
				}


			}
		}
		if (in_array($parameters['currentcontext'], array('thirdpartycard')))	    // do something only for the context 'somecontext1' or 'somecontext2'
		{
			if($action=='add'){
				$tva_intra = GETPOST('tva_intra', 'alphanohtml');

				$sql = 'SELECT rowid';
				$sql .= " FROM ".MAIN_DB_PREFIX."societe as s";
				if ($object->ismultientitymanaged == 1) $sql .= " WHERE entity IN (".getEntity($object->element).")";
				else $sql .= " WHERE 1 = 1";
				$sql .= " AND tva_intra = ".$tva_intra;

				$resql = $this->db->query($sql);

				if ($resql) {

					$num = $this->db->num_rows($resql);

						if ($num) {   // $num = 1
						$societe = new Societe($this->db);
						$obj = $this->db->fetch_object($resql);
						$societe->fetch($obj->rowid);

						$listsalesrepresentatives = $societe->getSalesRepresentatives($user);
						$nbofsalesrepresentative = count($listsalesrepresentatives);
						if ($nbofsalesrepresentative > 0 && is_array($listsalesrepresentatives)) {
							$userstatic = new User($this->db);
							foreach ($listsalesrepresentatives as $val) {
								$userstatic->id = $val['id'];
								$userstatic->login = $val['login'];
								$userstatic->lastname = $val['lastname'];
								$userstatic->firstname = $val['firstname'];
								$userstatic->statut = $val['statut'];
								$userstatic->photo = $val['photo'];
								$userstatic->email = $val['email'];
								$userstatic->phone = $val['phone'];
								$userstatic->job = $val['job'];
								$userstatic->entity = $val['entity'];
								$users  .= $userstatic->getNomUrl(-1);
								$users .= ' ';
							}

						}

						setEventMessages('Sus asignados comerciales son: '.$users, '', 'warnings');
					}

				}


			}

		}


		if (!$error) {
			$this->results = array('myreturn' => 999);
			$this->resprints = 'A text to show';
			return 0; // or return 1 to replace standard code
		} else {
			$this->errors[] = 'Error message';
			return -1;
		}
	}


	/**
	 * Overloading the doMassActions function : replacing the parent's function with the one below
	 *
	 * @param   array           $parameters     Hook metadatas (context, etc...)
	 * @param   CommonObject    $object         The object to process (an invoice if you are in invoice module, a propale in propale's module, etc...)
	 * @param   string          $action         Current action (if set). Generally create or edit or null
	 * @param   HookManager     $hookmanager    Hook manager propagated to allow calling another hook
	 * @return  int                             < 0 on error, 0 on success, 1 to replace standard code
	 */
	public function doMassActions($parameters, &$object, &$action, $hookmanager)
	{
		global $conf, $user, $langs;

		$error = 0; // Error counter

		/* print_r($parameters); print_r($object); echo "action: " . $action; */
		if (in_array($parameters['currentcontext'], array('somecontext1', 'somecontext2')))		// do something only for the context 'somecontext1' or 'somecontext2'
		{
			foreach ($parameters['toselect'] as $objectid)
			{
				// Do action on each object id
			}
		}

		if (!$error) {
			$this->results = array('myreturn' => 999);
			$this->resprints = 'A text to show';
			return 0; // or return 1 to replace standard code
		} else {
			$this->errors[] = 'Error message';
			return -1;
		}
	}


	/**
	 * Overloading the addMoreMassActions function : replacing the parent's function with the one below
	 *
	 * @param   array           $parameters     Hook metadatas (context, etc...)
	 * @param   CommonObject    $object         The object to process (an invoice if you are in invoice module, a propale in propale's module, etc...)
	 * @param   string          $action         Current action (if set). Generally create or edit or null
	 * @param   HookManager     $hookmanager    Hook manager propagated to allow calling another hook
	 * @return  int                             < 0 on error, 0 on success, 1 to replace standard code
	 */
	public function addMoreMassActions($parameters, &$object, &$action, $hookmanager)
	{
		global $conf, $user, $langs;

		$error = 0; // Error counter
		$disabled = 1;

		/* print_r($parameters); print_r($object); echo "action: " . $action; */


		if (in_array($parameters['currentcontext'], array('propalcard')))	    // do something only for the context 'somecontext1' or 'somecontext2'
		{
			if($action=='create'){
				$this->resprints = '<option value="0"'.($disabled ? ' disabled="disabled"' : '').'>'.$langs->trans("KimtechMassAction").'</option>';

				// print 'ola';
			}
		}

		if (!$error) {
			return 0; // or return 1 to replace standard code
		} else {
			$this->errors[] = 'Error message';
			return -1;
		}
	}



	/**
	 * Execute action
	 *
	 * @param	array	$parameters     Array of parameters
	 * @param   Object	$object		   	Object output on PDF
	 * @param   string	$action     	'add', 'update', 'view'
	 * @return  int 		        	<0 if KO,
	 *                          		=0 if OK but we want to process standard actions too,
	 *  	                            >0 if OK and we want to replace standard actions.
	 */
	public function beforePDFCreation($parameters, &$object, &$action)
	{
		global $conf, $user, $langs;
		global $hookmanager;

		$outputlangs = $langs;

		$ret = 0; $deltemp = array();
		dol_syslog(get_class($this).'::executeHooks action='.$action);

		/* print_r($parameters); print_r($object); echo "action: " . $action; */
		if (in_array($parameters['currentcontext'], array('somecontext1', 'somecontext2')))		// do something only for the context 'somecontext1' or 'somecontext2'
		{
		}

		return $ret;
	}

	/**
	 * Execute action
	 *
	 * @param	array	$parameters     Array of parameters
	 * @param   Object	$pdfhandler     PDF builder handler
	 * @param   string	$action         'add', 'update', 'view'
	 * @return  int 		            <0 if KO,
	 *                                  =0 if OK but we want to process standard actions too,
	 *                                  >0 if OK and we want to replace standard actions.
	 */
	public function afterPDFCreation($parameters, &$pdfhandler, &$action)
	{
		global $conf, $user, $langs;
		global $hookmanager;

		$outputlangs = $langs;

		$ret = 0; $deltemp = array();
		dol_syslog(get_class($this).'::executeHooks action='.$action);

		/* print_r($parameters); print_r($object); echo "action: " . $action; */
		if (in_array($parameters['currentcontext'], array('somecontext1', 'somecontext2'))) {
			// do something only for the context 'somecontext1' or 'somecontext2'
		}

		return $ret;
	}



	/**
	 * Overloading the loadDataForCustomReports function : returns data to complete the customreport tool
	 *
	 * @param   array           $parameters     Hook metadatas (context, etc...)
	 * @param   string          $action         Current action (if set). Generally create or edit or null
	 * @param   HookManager     $hookmanager    Hook manager propagated to allow calling another hook
	 * @return  int                             < 0 on error, 0 on success, 1 to replace standard code
	 */
	public function loadDataForCustomReports($parameters, &$action, $hookmanager)
	{
		global $conf, $user, $langs;

		$langs->load("kimtech@kimtech");

		$this->results = array();

		$head = array();
		$h = 0;

		if ($parameters['tabfamily'] == 'kimtech') {
			$head[$h][0] = dol_buildpath('/module/index.php', 1);
			$head[$h][1] = $langs->trans("Home");
			$head[$h][2] = 'home';
			$h++;

			$this->results['title'] = $langs->trans("Kimtech");
			$this->results['picto'] = 'kimtech@kimtech';
		}

		$head[$h][0] = 'customreports.php?objecttype='.$parameters['objecttype'].(empty($parameters['tabfamily']) ? '' : '&tabfamily='.$parameters['tabfamily']);
		$head[$h][1] = $langs->trans("CustomReports");
		$head[$h][2] = 'customreports';

		$this->results['head'] = $head;

		return 1;
	}



	/**
	 * Overloading the restrictedArea function : check permission on an object
	 *
	 * @param   array           $parameters     Hook metadatas (context, etc...)
	 * @param   string          $action         Current action (if set). Generally create or edit or null
	 * @param   HookManager     $hookmanager    Hook manager propagated to allow calling another hook
	 * @return  int 		      			  	<0 if KO,
	 *                          				=0 if OK but we want to process standard actions too,
	 *  	                            		>0 if OK and we want to replace standard actions.
	 */
	public function restrictedArea($parameters, &$action, $hookmanager)
	{
		global $user;

		if ($parameters['features'] == 'myobject') {
			if ($user->rights->kimtech->myobject->read) {
				$this->results['result'] = 1;
				return 1;
			} else {
				$this->results['result'] = 0;
				return 1;
			}
		}

		return 0;
	}

	/* Add here any other hooked methods... */
}
?>
