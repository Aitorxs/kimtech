<?php
/* Copyright (C) 2007-2017 Laurent Destailleur  <eldy@users.sourceforge.net>
 * Copyright (C) ---Put here your own copyright and developer email---
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

/**
 *   	\file       kimtech_list.php
 *		\ingroup    kimtech
 *		\brief      List page for kimtech
 */

//if (! defined('NOREQUIREDB'))              define('NOREQUIREDB', '1');				// Do not create database handler $db
//if (! defined('NOREQUIREUSER'))            define('NOREQUIREUSER', '1');				// Do not load object $user
//if (! defined('NOREQUIRESOC'))             define('NOREQUIRESOC', '1');				// Do not load object $mysoc
//if (! defined('NOREQUIRETRAN'))            define('NOREQUIRETRAN', '1');				// Do not load object $langs
//if (! defined('NOSCANGETFORINJECTION'))    define('NOSCANGETFORINJECTION', '1');		// Do not check injection attack on GET parameters
//if (! defined('NOSCANPOSTFORINJECTION'))   define('NOSCANPOSTFORINJECTION', '1');		// Do not check injection attack on POST parameters
//if (! defined('NOCSRFCHECK'))              define('NOCSRFCHECK', '1');				// Do not check CSRF attack (test on referer + on token if option MAIN_SECURITY_CSRF_WITH_TOKEN is on).
//if (! defined('NOTOKENRENEWAL'))           define('NOTOKENRENEWAL', '1');				// Do not roll the Anti CSRF token (used if MAIN_SECURITY_CSRF_WITH_TOKEN is on)
//if (! defined('NOSTYLECHECK'))             define('NOSTYLECHECK', '1');				// Do not check style html tag into posted data
//if (! defined('NOREQUIREMENU'))            define('NOREQUIREMENU', '1');				// If there is no need to load and show top and left menu
//if (! defined('NOREQUIREHTML'))            define('NOREQUIREHTML', '1');				// If we don't need to load the html.form.class.php
//if (! defined('NOREQUIREAJAX'))            define('NOREQUIREAJAX', '1');       	  	// Do not load ajax.lib.php library
//if (! defined("NOLOGIN"))                  define("NOLOGIN", '1');					// If this page is public (can be called outside logged session). This include the NOIPCHECK too.
//if (! defined('NOIPCHECK'))                define('NOIPCHECK', '1');					// Do not check IP defined into conf $dolibarr_main_restrict_ip
//if (! defined("MAIN_LANG_DEFAULT"))        define('MAIN_LANG_DEFAULT', 'auto');					// Force lang to a particular value
//if (! defined("MAIN_AUTHENTICATION_MODE")) define('MAIN_AUTHENTICATION_MODE', 'aloginmodule');	// Force authentication handler
//if (! defined("NOREDIRECTBYMAINTOLOGIN"))  define('NOREDIRECTBYMAINTOLOGIN', 1);		// The main.inc.php does not make a redirect if not logged, instead show simple error message
//if (! defined("FORCECSP"))                 define('FORCECSP', 'none');				// Disable all Content Security Policies
//if (! defined('CSRFCHECK_WITH_TOKEN'))     define('CSRFCHECK_WITH_TOKEN', '1');		// Force use of CSRF protection with tokens even for GET
//if (! defined('NOBROWSERNOTIF'))     		 define('NOBROWSERNOTIF', '1');				// Disable browser notification

// Load Dolibarr environment
$res = 0;
// Try main.inc.php into web root known defined into CONTEXT_DOCUMENT_ROOT (not always defined)
if (!$res && !empty($_SERVER["CONTEXT_DOCUMENT_ROOT"])) $res = @include $_SERVER["CONTEXT_DOCUMENT_ROOT"]."/main.inc.php";
// Try main.inc.php into web root detected using web root calculated from SCRIPT_FILENAME
$tmp = empty($_SERVER['SCRIPT_FILENAME']) ? '' : $_SERVER['SCRIPT_FILENAME']; $tmp2 = realpath(__FILE__); $i = strlen($tmp) - 1; $j = strlen($tmp2) - 1;
while ($i > 0 && $j > 0 && isset($tmp[$i]) && isset($tmp2[$j]) && $tmp[$i] == $tmp2[$j]) { $i--; $j--; }
if (!$res && $i > 0 && file_exists(substr($tmp, 0, ($i + 1))."/main.inc.php")) $res = @include substr($tmp, 0, ($i + 1))."/main.inc.php";
if (!$res && $i > 0 && file_exists(dirname(substr($tmp, 0, ($i + 1)))."/main.inc.php")) $res = @include dirname(substr($tmp, 0, ($i + 1)))."/main.inc.php";
// Try main.inc.php using relative path
if (!$res && file_exists("../main.inc.php")) $res = @include "../main.inc.php";
if (!$res && file_exists("../../main.inc.php")) $res = @include "../../main.inc.php";
if (!$res && file_exists("../../../main.inc.php")) $res = @include "../../../main.inc.php";
if (!$res) die("Include of main fails");

require_once DOL_DOCUMENT_ROOT.'/core/class/html.formcompany.class.php';
require_once DOL_DOCUMENT_ROOT.'/core/lib/date.lib.php';
require_once DOL_DOCUMENT_ROOT.'/core/lib/company.lib.php';
require_once DOL_DOCUMENT_ROOT.'/societe/class/societe.class.php';
require_once DOL_DOCUMENT_ROOT.'/user/class/user.class.php';

// load kimtech libraries
require_once __DIR__.'/class/kimtech.class.php';

// for other modules
//dol_include_once('/othermodule/class/otherobject.class.php');

// Load translation files required by the page
$langs->loadLangs(array("kimtech@kimtech", "propal"));

$action     = GETPOST('action', 'aZ09') ?GETPOST('action', 'aZ09') : ''; // The action 'add', 'create', 'edit', 'update', 'view', ...
$backtopage = GETPOST('backtopage', 'alpha');
$cancel		= GETPOST('cancel', 'alpha');
$tva_intra = GETPOST('tva_intra', 'aZ09');
$tipodocumento = GETPOST('tipodocumento', 'int');


if ($cancel)
{
	$action = '';
	if (!empty($backtopage))
	{
		$backtopage = urlencode($_SERVER["PHP_SELF"].'?tva_intra='.$tva_intra.'&tipodocumento='.$tipodocumento);

		header("Location: ".$backtopage);
		exit;
	}
}

global $conf, $user, $langs;

// Initialize technical objects
// $object = new Kimtech($db);
// $extrafields = new ExtraFields($db);
$societe = new Societe($db);
$diroutputmassaction = $conf->kimtech->dir_output.'/temp/massgeneration/'.$user->id;
$hookmanager->initHooks(array('kimtechlist')); // Note that conf->hooks_modules contains array

// Fetch optionals attributes and labels
// $extrafields->fetch_name_optionals_label($object->table_element);
// //$extrafields->fetch_name_optionals_label($object->table_element_line);

// $search_array_options = $extrafields->getOptionalsFromPost($object->table_element, '', 'search_');

// // Default sort order (if not yet defined by previous GETPOST)
// if (!$sortfield) { reset($object->fields); $sortfield="t.".key($object->fields); }   // Set here default search field. By default 1st field in definition. Reset is required to avoid key() to return null.
// if (!$sortorder) $sortorder = "ASC";

// // Initialize array of search criterias
// $search_all = GETPOST('search_all', 'alphanohtml') ? GETPOST('search_all', 'alphanohtml') : GETPOST('sall', 'alphanohtml');
// $search = array();
// foreach ($object->fields as $key => $val)
// {
// 	if (GETPOST('search_'.$key, 'alpha') !== '') $search[$key] = GETPOST('search_'.$key, 'alpha');
// }

// // List of fields to search into when doing a "search in all"
// $fieldstosearchall = array();
// foreach ($object->fields as $key => $val)
// {
// 	if ($val['searchall']) $fieldstosearchall['t.'.$key] = $val['label'];
// }

// // Definition of array of fields for columns
// $arrayfields = array();
// foreach ($object->fields as $key => $val) {
// 	// If $val['visible']==0, then we never show the field
// 	if (!empty($val['visible'])) {
// 		$visible = (int) dol_eval($val['visible'], 1);
// 		$arrayfields['t.'.$key] = array(
// 			'label'=>$val['label'],
// 			'checked'=>(($visible < 0) ? 0 : 1),
// 			'enabled'=>($visible != 3 && dol_eval($val['enabled'], 1)),
// 			'position'=>$val['position'],
// 			'help'=>$val['help']
// 		);
// 	}
// }
// Extra fields
include DOL_DOCUMENT_ROOT.'/core/tpl/extrafields_list_array_fields.tpl.php';

// $object->fields = dol_sort_array($object->fields, 'position');
// $arrayfields = dol_sort_array($arrayfields, 'position');

$permissiontoread = $user->rights->kimtech->kimtech->read;
$permissiontoadd = $user->rights->societe->creer;
$permissiontodelete = $user->rights->kimtech->kimtech->delete;

// Security check
if (empty($conf->kimtech->enabled)) accessforbidden('Module not enabled');
$socid = 0;
if ($user->socid > 0)	// Protection if external user
{
	//$socid = $user->socid;
	accessforbidden();
}
//$result = restrictedArea($user, 'kimtech', $id, '');
//if (!$permissiontoread) accessforbidden();


/*
 * View
 */

$form = new Form($db);

$now = dol_now();

//$help_url="EN:Module_Kimtech|FR:Module_Kimtech_FR|ES:Módulo_Kimtech";
$help_url = '';
$title = $langs->trans('SearchSociete', $langs->transnoentitiesnoconv("Kimtechs"));



// Output page
// --------------------------------------------------------------------

llxHeader('', $title, $help_url);

print '<form method="POST" id="searchFormList" action="'.$_SERVER["PHP_SELF"].'">'."\n";
print '<input type="hidden" name="token" value="'.newToken().'">';
print '<input type="hidden" name="action" value="search">';
print '<input type="hidden" name="backtopage" value="'.$backtopage.'">';

//$url = DOL_URL_ROOT.'/societe/card.php?action=create&type=c&tva_intra='.$tva_intra.'&options_tipodocumento='.$tipodocumento.'&customer_code=CL01-'.$tva_intra.'&backtopage='.urlencode($_SERVER["PHP_SELF"].'?tva_intra='.$tva_intra.'&tipodocumento='.$tipodocumento);

$url = DOL_URL_ROOT.'/societe/card.php?action=create&type=c&tva_intra='.$tva_intra.'&options_tipodocumento='.$tipodocumento.'&customer_code=CL01-'.$tva_intra;
$newcardbutton = dolGetButtonTitle($langs->trans('New'), '', 'fa fa-plus-circle',$url, '', $permissiontoadd);


print_barre_liste($title, $page, $_SERVER["PHP_SELF"], $param, $sortfield, $sortorder, $massactionbutton, $num, $nbtotalofrecords, '', 0, $newcardbutton, '', '', 0, 0, 1);


print '<div class="div-table">'; // You can use div-table-responsive-no-min if you dont need reserved height for your table
print '<table class="tagtable nobottomiftotal liste'.($moreforfilter ? " listwithfilterbefore" : "").'">'."\n";
print'<tr>
		<td class="fieldrequired">Tipo Documento</td>
		<td>
			<select class="flat minwidth100 maxwidthonsmartphone" name="tipodocumento" id="tipodocumento">
				<option value="0">&nbsp;</option><option value="4">CE</option>
				<option value="1" >DNI </option>
				<option value="99">OTROS DOC</option>
				<option value="6" selected="">RUC</option>
			</select>
		</td>
		</tr>
		<tr>

		<td class="fieldrequired">N. de documento</td>
		<td class="nowrap" colspan="3">
			<input type="text"  class="flat maxwidthonsmartphone" name="tva_intra" id="intra_vat" placeholder="00000000000" value="'.$tva_intra.'" autofocus="autofocus" onblur="validarnumerodoc()" >
			<button class="button" name="btn-submit" id="btn-submit">
			<i class="fa fa-search"></i> Consultar</button><br><span id="errorlongitud" style="display:none;"><font color="red">Error longitud documento</font></span>
		</td></tr>' ;

print '</table>'."\n";
print '</div>'."\n";

print '</form>'."<br><br><br>";



if($action == 'search'){

	// Build and execute select
	// --------------------------------------------------------------------
	$sql = 'SELECT rowid,datec';
	$sql .= " FROM ".MAIN_DB_PREFIX."societe as s";

	// if ($object->ismultientitymanaged == 1) $sql .= " WHERE t.entity IN (".getEntity($object->element).")";
	$sql .= " WHERE 1 = 1";
	$sql .= " AND tva_intra = ".$tva_intra;


	$resql = $db->query($sql);
	print '<div class="div-table-responsive">';

	if ($resql) {

		$num = $db->num_rows($resql);



		if ($num) {   // $num = 1
			$obj = $db->fetch_object($resql);
			$societe->fetch($obj->rowid);

			print 'El N° de documento  <b>'.$tva_intra .' </b> se encuentra registrado con la empresa <b>'.$societe->getFullName($langs)."</b><br><br>";

			print 'Sus asignados comerciales son: ';
			$listsalesrepresentatives = $societe->getSalesRepresentatives($user);
			$nbofsalesrepresentative = count($listsalesrepresentatives);
			if ($nbofsalesrepresentative > 0 && is_array($listsalesrepresentatives)) {
				$userstatic = new User($db);
				foreach ($listsalesrepresentatives as $val) {
					$userstatic->id = $val['id'];
					$userstatic->login = $val['login'];
					$userstatic->lastname = $val['lastname'];
					$userstatic->firstname = $val['firstname'];
					$userstatic->statut = $val['statut'];
					$userstatic->photo = $val['photo'];
					$userstatic->email = $val['email'];
					$userstatic->phone = $val['phone'];
					$userstatic->job = $val['job'];
					$userstatic->entity = $val['entity'];
					print $userstatic->getNomUrl(-1);
					print ' ';
				}
			}else{
				print ' No tiene ';
			}


		}else{
			print 'El N° de documento <b>'.$tva_intra .'</b> no se encuentra registrado en la base de datos';

		}

		//mostrar mensaje de 4 meses
		if($num){

			$socid = GETPOST('socid', 'int')?GETPOST('socid', 'int'): $obj->rowid;

			$sql1 = 'SELECT rowid,datep';
			$sql1 .= " FROM ".MAIN_DB_PREFIX."propal";
			$sql1 .= " WHERE fk_soc= ".$socid;
			$sql1 .= " order by rowid desc limit 1 ";

			$resql1 = $db->query($sql1);
			if ($resql1) {

				$obj1 = $db->fetch_object($resql1);
				$num1 = $db->num_rows($resql1);

				if ($num1) {

					$datetime1=new DateTime($obj1->datep);
					$datetime2=new DateTime('now');
					$interval=$datetime2->diff($datetime1);
					$intervalMeses=$interval->format("%m");
					$intervalAnos = $interval->format("%y")*12;
					$cant_mes = ($intervalMeses+$intervalAnos);

					if($cant_mes >= 4){
						echo "<br><br><p style='color:  #ab0000'>Empresa no cotizada en un periodo mayor de 4 meses, comuníquese con el administrador para solicitar permiso de cotizar.</p><br>";
					}
					//echo "*".$obj1->datep;
				}
			}
		}

		// mostrar los presupuestos
		if($num1){

			require_once DOL_DOCUMENT_ROOT.'/core/class/html.formother.class.php';
			require_once DOL_DOCUMENT_ROOT.'/core/class/html.formfile.class.php';
			require_once DOL_DOCUMENT_ROOT.'/core/class/html.formpropal.class.php';
			require_once DOL_DOCUMENT_ROOT.'/core/class/html.formcompany.class.php';
			require_once DOL_DOCUMENT_ROOT.'/core/lib/date.lib.php';
			require_once DOL_DOCUMENT_ROOT.'/core/lib/company.lib.php';
			require_once DOL_DOCUMENT_ROOT.'/comm/propal/class/propal.class.php';

			// Load translation files required by the page


			$socid = GETPOST('socid', 'int')?GETPOST('socid', 'int'): $obj->rowid;



			$limit = GETPOST('limit', 'int') ?GETPOST('limit', 'int') : 3;

			$page = GETPOSTISSET('pageplusone') ? (GETPOST('pageplusone') - 1) : GETPOST("page", 'int');
			if (empty($page) || $page == -1 || !empty($search_btn) || !empty($search_remove_btn) || (empty($toselect) && $massaction === '0')) { $page = 0; }     // If $page is not defined, or '' or -1
			$offset = $limit * $page;
			$pageprev = $page - 1;
			$pagenext = $page + 1;
			if (!$sortfield) $sortfield = 'p.datep';
			if (!$sortorder) $sortorder = 'DESC';


			// Security check
			$module = 'propal';
			$dbtable = '';
			$objectid = '';
			if (!empty($user->socid))	$socid = $user->socid;
			if (!empty($socid))
			{
				$objectid = $socid;
				$module = 'societe';
				$dbtable = '&societe';
			}
			// $result = restrictedArea($user, $module, $objectid, $dbtable);

			$diroutputmassaction = $conf->propal->multidir_output[$conf->entity].'/temp/massgeneration/'.$user->id;

			// Initialize technical object to manage hooks of page. Note that conf->hooks_modules contains array of hook context
			$object = new Propal($db);
			// $hookmanager->initHooks(array('propallist'));
			$extrafields = new ExtraFields($db);

			// fetch optionals attributes and labels
			$extrafields->fetch_name_optionals_label($object->table_element);

			$search_array_options = $extrafields->getOptionalsFromPost($object->table_element, '', 'search_');




			$checkedtypetiers = 0;
			$arrayfields = array(
				'p.ref'=>array('label'=>"Ref", 'checked'=>1),

				'p.date'=>array('label'=>"DatePropal", 'checked'=>1),
				'p.multicurrency_total_ttc'=>array('label'=>'MulticurrencyAmountTTC', 'checked'=>0, 'enabled'=>(empty($conf->multicurrency->enabled) ? 0 : 1)),
				'u.login'=>array('label'=>"Author", 'checked'=>1, 'position'=>10),
				'vendedor'=>array('label'=>"Vendedor", 'checked'=>1, 'position'=>10),
				'p.model_pdf' => array('label'=>"Empresa", 'checked'=>1, 'position'=>10),
				'p.total_ttc'=>array('label'=>"AmountTTC", 'checked'=>0),
				'p.multicurrency_code'=>array('label'=>'Currency', 'checked'=>0, 'enabled'=>(empty($conf->multicurrency->enabled) ? 0 : 1)),



				'p.fk_statut'=>array('label'=>"Status", 'checked'=>1, 'position'=>1000),
			);
			// Extra fields
			// include DOL_DOCUMENT_ROOT.'/core/tpl/extrafields_list_array_fields.tpl.php';


			/*
			 * View
			 */

			$now = dol_now();

			$form = new Form($db);
			$formother = new FormOther($db);
			$formfile = new FormFile($db);
			$formpropal = new FormPropal($db);
			$companystatic = new Societe($db);

			$formcompany = new FormCompany($db);

			$help_url = 'EN:Commercial_Proposals|FR:Proposition_commerciale|ES:Presupuestos';
			//llxHeader('',$langs->trans('Proposal'),$help_url);

			$sql = 'SELECT';
			if ($sall || $search_product_category > 0 || $search_user > 0) $sql = 'SELECT DISTINCT';
			$sql .= ' s.rowid as socid, s.nom as name, s.name_alias, s.email, s.town, s.zip, s.fk_pays, s.client, s.code_client, ';

			$sql .= ' p.rowid, p.entity, p.note_private, p.total_ht, p.tva as total_vat, p.total as total_ttc, p.localtax1, p.localtax2, p.ref, p.ref_client, p.fk_statut as status, p.fk_user_author, p.datep as dp, p.fin_validite as dfv,p.date_livraison as ddelivery,';
			$sql .= ' p.fk_multicurrency, p.multicurrency_code, p.multicurrency_tx, p.multicurrency_total_ht, p.multicurrency_total_tva as multicurrency_total_vat, p.multicurrency_total_ttc,';
			$sql .= ' p.datec as date_creation, p.tms as date_update, p.date_cloture as date_cloture,';
			$sql .= ' p.note_public, p.note_private,';
			$sql .= 'p.model_pdf, ef.getsales,';
			$sql .= ' u.login';
			if (!$user->rights->societe->client->voir && !$socid) $sql .= ", sc.fk_soc, sc.fk_user";
			if ($search_categ_cus) $sql .= ", cc.fk_categorie, cc.fk_soc";
			// Add fields from extrafields
			if (!empty($extrafields->attributes[$object->table_element]['label'])) {
				foreach ($extrafields->attributes[$object->table_element]['label'] as $key => $val) $sql .= ($extrafields->attributes[$object->table_element]['type'][$key] != 'separate' ? ", ef.".$key.' as options_'.$key : '');
			}
			$sql .= ' FROM '.MAIN_DB_PREFIX.'societe as s';
			if (!empty($search_categ_cus)) $sql .= ' LEFT JOIN '.MAIN_DB_PREFIX."categorie_societe as cc ON s.rowid = cc.fk_soc"; // We'll need this table joined to the select in order to filter by categ
			$sql .= ', '.MAIN_DB_PREFIX.'propal as p';
			if (is_array($extrafields->attributes[$object->table_element]['label']) && count($extrafields->attributes[$object->table_element]['label'])) $sql .= " LEFT JOIN ".MAIN_DB_PREFIX.$object->table_element."_extrafields as ef on (p.rowid = ef.fk_object)";
			if ($sall || $search_product_category > 0) $sql .= ' LEFT JOIN '.MAIN_DB_PREFIX.'propaldet as pd ON p.rowid=pd.fk_propal';
			$sql .= ' LEFT JOIN '.MAIN_DB_PREFIX.'user as u ON p.fk_user_author = u.rowid';


			// We'll need this table joined to the select in order to filter by sale
			if ($search_sale > 0 || (!$user->rights->societe->client->voir && !$socid)) $sql .= ", ".MAIN_DB_PREFIX."societe_commerciaux as sc";
			if ($search_user > 0)
			{
				$sql .= ", ".MAIN_DB_PREFIX."element_contact as c";
				$sql .= ", ".MAIN_DB_PREFIX."c_type_contact as tc";
			}
			$sql .= ' WHERE p.fk_soc = s.rowid';
			$sql .= ' AND p.entity IN ('.getEntity('propal').')';
			if (!$user->rights->societe->client->voir && !$socid) //restriction
			{
				$sql .= " AND s.rowid = sc.fk_soc AND sc.fk_user = ".$user->id;
			}



			if ($socid > 0) $sql .= ' AND s.rowid = '.$socid;

			// Add where from extra fields
			include DOL_DOCUMENT_ROOT.'/core/tpl/extrafields_list_search_sql.tpl.php';

			// Add where from hooks
			// $parameters = array();
			// $reshook = $hookmanager->executeHooks('printFieldListWhere', $parameters); // Note that $action and $object may have been modified by hook
			$sql .= $hookmanager->resPrint;

			$sql .= $db->order($sortfield, $sortorder);

			$sql .= $db->plimit($limit + 1, $offset);

			$resql = $db->query($sql);

			if ($resql)
			{
				$objectstatic = new Propal($db);
				$userstatic = new User($db);

				if ($socid > 0)
				{
					$soc = new Societe($db);
					$soc->fetch($socid);
					$title = $langs->trans('ListOfProposals').' - '.$soc->name;
					if (empty($search_societe)) $search_societe = $soc->name;
				} else {
					$title = $langs->trans('ListOfProposals');
				}

				$num = $db->num_rows($resql);

				$arrayofselected = is_array($toselect) ? $toselect : array();

				// Fields title search
				print '<form method="POST" id="searchFormList" action="'.$_SERVER["PHP_SELF"].'">';
				if ($optioncss != '') print '<input type="hidden" name="optioncss" value="'.$optioncss.'">';
				print '<input type="hidden" name="token" value="'.newToken().'">';
				print '<input type="hidden" name="formfilteraction" id="formfilteraction" value="list">';
				print '<input type="hidden" name="action" value="list">';


				print_barre_liste($title, 0, $_SERVER["PHP_SELF"], $param, $sortfield, $sortorder, $massactionbutton, 0, $nbtotalofrecords, 'propal', 0, '', '', $limit, 0, 0, '');

				print '<div class="div-table-responsive">';
				print '<table class="tagtable liste'.($moreforfilter ? " listwithfilterbefore" : "").'">'."\n";


				// Fields title
				print '<tr class="liste_titre">';
				print '<td class="nowraponall">Referencia</td>' ;
				print '<td class="nowraponall">Fecha</td>' ;
				print '<td class="nowraponall">Autor</td>' ;
				print '<td class="nowraponall">Vendedor</td>' ;
				print '<td class="nowraponall">Empresa</td>' ;

				print '</tr>'."\n";

				$now = dol_now();
				$i = 0;
				$totalarray = array();
				$typenArray = null;

				while ($i < min($num, $limit))
				{
					$obj = $db->fetch_object($resql);

					$objectstatic->id = $obj->rowid;
					$objectstatic->ref = $obj->ref;

					$objectstatic->statut = $obj->status;
					$objectstatic->status = $obj->status;

					$companystatic->id = $obj->socid;


					print '<tr class="oddeven">';


					print '<td class="nowraponall">';
					// Picto + Ref
					print $obj->ref;
					print "</td>\n";
					if (!$i) $totalarray['nbfield']++;


					// Date proposal
					print '<td class="nowraponall">';
					print dol_print_date($db->jdate($obj->dp), 'day');
					print "</td>\n";
					if (!$i) $totalarray['nbfield']++;


					$userstatic->id = $obj->fk_user_author;
					$userstatic->login = $obj->login;

					// Author
					print '<td class=" nowraponall">';
					if ($userstatic->id) print $obj->login;
					print "</td>\n";
					if (!$i) $totalarray['nbfield']++;


					//vendedor
					print '<td class=" nowraponall">';
					$userstatic2 = new User($db);
					$userstatic2->fetch($obj->getsales);
					print $userstatic2->firstname.' '.$userstatic2->lastname;
					print "</td>\n";
					if (!$i) $totalarray['nbfield']++;


					// Fields from hook
					print $hookmanager->resPrint;
					print '<td class="nowrap">'.$obj->model_pdf."</td>\n";
					if (!$i) $totalarray['nbfield']++;

					$i++;
				}

				$db->free($resql);
				print '</tr>';

				print '</table>'."\n";
				print '</div>'."\n";

				print '</form>'."\n";




			} else {
				dol_print_error($db);
			}

		}else
		{
			print '<br><br>La empresa no cuenta con ningún presupuesto asignado, la fecha de registro del cliente es <b>'.dol_print_date($db->jdate($obj->datec ), 'day');
		}


	}
	print '</div>'."\n";




}

// End of page
llxFooter();
$db->close();
